import { create } from 'zustand';
import { persist, createJSONStorage } from 'zustand/middleware';
import { StakingInfo, ValidatorInfo } from '@src/types';
import { BigNumber } from 'ethers';

interface StakingState {
  validators: ValidatorInfo[];
  validatorsLoading: boolean;
  stakingInfo: StakingInfo;
  stakingInfoLoading: boolean;

  setValidators: (validators: ValidatorInfo[]) => void;
  setStakingInfo: (stakingInfo: StakingInfo) => void;
  setStakingInfoLoading: () => void;
}

export const defaultStakingInfo: StakingInfo = {
  validator: '',
  stake: BigNumber.from(0),
  status: 0,
  withdrawalAmount: BigNumber.from(0),
  withdrawalTime: BigNumber.from(0),
};

export const useStakingStore = create<StakingState>()(
  persist(
    (set) => ({
      validators: [],
      validatorsLoading: true,
      stakingInfo: defaultStakingInfo,
      stakingInfoLoading: true,

      setValidators: (validators) => set({ validators, validatorsLoading: false }),
      setStakingInfo: (stakingInfo) =>
        set({
          stakingInfo: {
            ...stakingInfo,
            withdrawalTime: BigNumber.from(stakingInfo.withdrawalTime),
            withdrawalAmount: BigNumber.from(stakingInfo.withdrawalAmount),
          },
          stakingInfoLoading: false,
        }),
      setStakingInfoLoading: () => set({ stakingInfo: defaultStakingInfo, stakingInfoLoading: true }),
    }),
    {
      name: 'staking-store',
      storage: createJSONStorage(() => localStorage),
      partialize: (state) => ({
        stakingInfo: state.stakingInfo,
        validators: state.validators,
      }),
    },
  ),
);
