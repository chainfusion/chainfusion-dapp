import { create } from 'zustand';
import { persist, createJSONStorage } from 'zustand/middleware';
import { BridgeTransfer } from '@src/types';
import { BigNumber } from 'ethers';

interface BridgeState {
  receiver?: string;
  history: BridgeTransfer[];
  historyLoading: boolean;
  historyItemsToShow: number;
  onlyMyHistory: boolean;

  setReceiver: (receiver?: string) => void;
  setHistory: (history: BridgeTransfer[]) => void;
  setHistoryLoading: (historyLoading: boolean) => void;
  setHistoryItemsToShow: (itemsToShow: number) => void;
  setOnlyMyHistory: (onlyMyHistory: boolean) => void;
}

export const useBridgeStore = create(
  persist<BridgeState>(
    (set) => ({
      receiver: undefined,
      history: [],
      historyLoading: true,
      historyItemsToShow: 5,
      onlyMyHistory: false,

      setReceiver: (receiver) => set({ receiver }),
      setHistory: (history) => set({ history }),
      setHistoryLoading: (historyLoading) =>
        set((state) => ({
          historyLoading,
          historyItemsToShow: historyLoading ? 5 : state.historyItemsToShow,
        })),
      setHistoryItemsToShow: (itemsToShow) => set({ historyItemsToShow: itemsToShow }),
      setOnlyMyHistory: (onlyMyHistory) => set({ onlyMyHistory }),
    }),
    {
      name: 'bridge-store',
      storage: createJSONStorage(() => localStorage),
      partialize: (state) => ({
        ...state,
        history: state.history.map((item) => ({
          ...item,
          amount: BigNumber.from(item.amount),
          fee: BigNumber.from(item.fee),
        })),
      }),
    },
  ),
);
