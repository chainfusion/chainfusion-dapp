import {
  EventRegistry,
  EventRegistry__factory,
  RelayBridge,
  RelayBridge__factory,
  Staking,
  Staking__factory,
} from '@chainfusion/chainfusion-contracts';
import {
  ERC20Bridge,
  ERC20Bridge__factory,
  FeeManager,
  FeeManager__factory,
  LiquidityPools,
  LiquidityPools__factory,
  TokenManager,
  TokenManager__factory,
} from '@chainfusion/erc-20-bridge-contracts';
import { getNativeChain, getNativeContracts, getSupportedChains } from '@src/config';
import { Chain } from '@src/types';
import {
  Connector,
  useAccount,
  useConnect,
  useNetwork,
  usePublicClient,
  useSwitchNetwork,
  useWalletClient,
} from 'wagmi';
import { ethers, providers } from 'ethers';
import { createContext, ReactElement, useContext, useEffect, useMemo, useState } from 'react';
import { useDisclosure } from '@chakra-ui/react';
import { WalletConnectModal } from '@components/Modals/WalletConnectModal';

const defaultAccount = '0x0000000000000000000000000000000000000001';

export interface NativeContainer {
  provider: providers.JsonRpcProvider;
  account: string;
  connected: boolean;

  staking: Staking;
  eventRegistry: EventRegistry;
}

export interface NetworkContainer {
  [key: string]: ChainNetwork | undefined;
}

export interface AddressContainer {
  [key: string]: ChainAddresses | undefined;
}

export interface ChainNetwork {
  chain: Chain;
  provider: providers.JsonRpcProvider;
  account: string;
  connected: boolean;

  contracts?: ChainContracts;
}

export interface ChainAddresses {
  erc20Bridge: string;
  relayBridge: string;
  tokenManager: string;
  liquidityPools: string;
  feeManager: string;
}

export interface ChainContracts {
  erc20Bridge: ERC20Bridge;
  relayBridge: RelayBridge;
  tokenManager: TokenManager;
  liqidityPools: LiquidityPools;
  feeManager: FeeManager;
}

export interface ChainContextData {
  signerAccount: string;
  nativeContainer?: NativeContainer;
  networkContainer: Map<string, ChainNetwork>;
  addressContainer?: AddressContainer;
  switchNetwork: (chain: Chain) => Promise<void>;
  showConnectWalletDialog: (chain?: Chain) => void;
}

export const ChainContext = createContext({} as ChainContextData);
export const useChainContext = () => useContext(ChainContext);

export interface ChainContextProviderProps {
  children: ReactElement;
}

export const ChainContextProvider = ({ children }: ChainContextProviderProps) => {
  const { chain } = useNetwork();
  const { address: account, isConnected } = useAccount();
  const { data: walletClient } = useWalletClient();
  const { switchNetwork: wagmiSwitchNetwork } = useSwitchNetwork();
  const chainId = chain?.id;

  const { open: openConnectModal, onOpen: onOpenConnectModal, onClose } = useDisclosure();
  const { connect, connectors, isLoading } = useConnect();

  const publicClient = usePublicClient();

  const initialNetworkContainer: NetworkContainer = {};
  const chains = getSupportedChains();
  for (const chain of chains) {
    const provider = new ethers.providers.StaticJsonRpcProvider(chain.rpc, chain.chainId);

    initialNetworkContainer[chain.identifier] = {
      chain: chain,
      provider: provider,
      account: defaultAccount,
      connected: false,
    };
  }

  const [desiredChain, setDesiredChain] = useState<Chain>();
  const [addressContainer, setAddressContainer] = useState<AddressContainer | undefined>(undefined);
  const [networkContainer, setNetworkContainer] = useState(new Map<string, ChainNetwork>());
  const [nativeContainer, setNativeContainer] = useState<NativeContainer | undefined>(undefined);

  const provider = useMemo(() => {
    if (walletClient) {
      return new ethers.providers.Web3Provider(walletClient.transport);
    } else if (publicClient?.transport?.url) {
      return new ethers.providers.JsonRpcProvider(publicClient.transport.url);
    } else if (typeof window !== 'undefined' && window.ethereum) {
      return new ethers.providers.Web3Provider(window.ethereum);
    }
    return undefined;
  }, [walletClient, publicClient]);

  // const signer = useMemo(() => {
  //   return provider ? provider.getSigner() : undefined;
  // }, [provider]);

  const signerAccount = account ?? defaultAccount;

  useEffect(() => {
    try {
      const nativeChain = getNativeChain();
      const nativeContracts = getNativeContracts();

      let connected = false;
      let nativeProvider = new providers.StaticJsonRpcProvider(nativeChain.rpc, nativeChain.chainId);
      if (provider !== undefined && chainId === nativeChain.chainId && isConnected) {
        nativeProvider = provider;
        connected = true;
      }

      const stakingFactory = new Staking__factory(nativeProvider.getSigner(signerAccount));
      const staking = stakingFactory.attach(nativeContracts.staking);

      const eventRegistryFactory = new EventRegistry__factory(nativeProvider.getSigner(signerAccount));
      const eventRegistry = eventRegistryFactory.attach(nativeContracts.eventRegistry);

      setNativeContainer({
        account: signerAccount,
        provider: nativeProvider,
        connected,
        staking,
        eventRegistry,
      });
    } catch {
      setNativeContainer(undefined);
    }
  }, [signerAccount, chainId, provider, isConnected]);

  useEffect(() => {
    let isMounted = true;

    const loadChainAddresses = async (chain: Chain): Promise<ChainAddresses | undefined> => {
      if (!chain.erc20BridgeAddress) return undefined;

      try {
        const provider = new providers.StaticJsonRpcProvider(chain.rpc, chain.chainId);
        const signer = provider.getSigner(signerAccount);

        const erc20Bridge = new ERC20Bridge__factory(signer).attach(chain.erc20BridgeAddress);

        const [relayBridge, tokenManager, liquidityPools, feeManager] = await Promise.all([
          erc20Bridge.relayBridge(),
          erc20Bridge.tokenManager(),
          erc20Bridge.liquidityPools(),
          erc20Bridge.feeManager(),
        ]);

        return { erc20Bridge: chain.erc20BridgeAddress, relayBridge, tokenManager, liquidityPools, feeManager };
      } catch (error) {
        console.error(`❌ Error loading data for ${chain.identifier}:`, error);
        return undefined;
      }
    };

    const loadAddressContainer = async () => {
      const results = await Promise.all(chains.map(loadChainAddresses));
      const addressContainer: AddressContainer = {};

      chains.forEach((chain, index) => {
        if (results[index]) {
          addressContainer[chain.identifier] = results[index];
        }
      });

      if (isMounted) {
        setAddressContainer(addressContainer);
      }
    };

    loadAddressContainer().then();

    return () => {
      isMounted = false;
    };
  }, [chains, signerAccount]);

  const networkContainerLocal = useMemo(() => {
    if (!addressContainer) return new Map<string, ChainNetwork>();

    const container = new Map<string, ChainNetwork>();

    chains.forEach((chain) => {
      const chainProvider =
        chain.chainId === chainId && provider && isConnected
          ? provider
          : new providers.StaticJsonRpcProvider(chain.rpc, chain.chainId);

      const chainAddresses = addressContainer[chain.identifier];
      if (!chainAddresses) return;

      const signer = chainProvider.getSigner(signerAccount);

      const chainContracts: ChainContracts = {
        erc20Bridge: new ERC20Bridge__factory(signer).attach(chainAddresses.erc20Bridge),
        relayBridge: new RelayBridge__factory(signer).attach(chainAddresses.relayBridge),
        tokenManager: new TokenManager__factory(signer).attach(chainAddresses.tokenManager),
        liqidityPools: new LiquidityPools__factory(signer).attach(chainAddresses.liquidityPools),
        feeManager: new FeeManager__factory(signer).attach(chainAddresses.feeManager),
      };

      container.set(chain.identifier, {
        chain,
        provider: chainProvider,
        account: signerAccount,
        connected: chain.chainId === chainId && isConnected,
        contracts: chainContracts,
      });
    });

    return container;
  }, [signerAccount, chains, chainId, provider, addressContainer, isConnected]);

  useEffect(() => {
    setNetworkContainer(networkContainerLocal);
  }, [networkContainerLocal]);

  const switchNetwork = async (chain: Chain) => {
    wagmiSwitchNetwork?.(chain?.chainId);
  };

  const showConnectWalletDialog = (chain?: Chain) => {
    setDesiredChain(chain);
    onOpenConnectModal();
  };

  return (
    <ChainContext.Provider
      value={{
        signerAccount,
        nativeContainer,
        networkContainer,
        addressContainer,
        switchNetwork,
        showConnectWalletDialog,
      }}
    >
      {children}
      <WalletConnectModal
        isOpen={openConnectModal}
        onClose={onClose}
        isLoading={isLoading}
        connect={connect}
        connectors={connectors as Connector[]}
        pendingConnector={connectors.find((c) => isLoading && c.id)}
        desiredChain={desiredChain}
      />
    </ChainContext.Provider>
  );
};
