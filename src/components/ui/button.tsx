'use client';

import * as React from 'react';
import { Button as ChakraButton, Spinner, useRecipe, Center } from '@chakra-ui/react';
import { button } from '@/lib/chakra/recipes/button';
import type { RecipeVariantProps } from '@chakra-ui/react';

type ButtonVariantProps = RecipeVariantProps<typeof button>;

interface ButtonProps extends Omit<React.ComponentProps<typeof ChakraButton>, 'size' | 'variant'>, ButtonVariantProps {
  loading?: boolean;
  loadingText?: React.ReactNode;
}

export const Button = React.forwardRef<HTMLButtonElement, ButtonProps>((props, ref) => {
  const { variant = 'solid', size = 'lg', loading, disabled, loadingText, children, ...rest } = props;
  const recipe = useRecipe({ recipe: button });

  const styles = recipe({ variant: variant as 'solid' | 'outline' | 'ghost', size: size as 'sm' | 'lg' });

  return (
    <ChakraButton css={styles} disabled={loading || disabled} ref={ref} {...rest}>
      {loading ? (
        <>
          <Center display="inline-flex">
            <Spinner size="sm" color="inherit" />
          </Center>
          {loadingText && <span>{loadingText}</span>}
        </>
      ) : (
        children
      )}
    </ChakraButton>
  );
});

Button.displayName = 'Button';
