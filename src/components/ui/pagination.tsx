'use client';

import type { ButtonProps } from '@chakra-ui/react';
import {
  Button,
  Pagination as ChakraPagination,
  IconButton,
  createContext,
  usePaginationContext,
} from '@chakra-ui/react';
import * as React from 'react';
import { HiChevronLeft, HiChevronRight, HiMiniEllipsisHorizontal } from 'react-icons/hi2';

interface ButtonVariantMap {
  current: ButtonProps['variant'];
  default: ButtonProps['variant'];
  ellipsis: ButtonProps['variant'];
}

type PaginationVariant = 'custom';

interface ButtonVariantContext {
  size: ButtonProps['size'];
  variantMap: ButtonVariantMap;
}

const [RootPropsProvider, useRootProps] = createContext<ButtonVariantContext>({
  name: 'RootPropsProvider',
});

export interface PaginationRootProps extends Omit<ChakraPagination.RootProps, 'type'> {
  size?: ButtonProps['size'];
  variant?: PaginationVariant;
}

const variantMap: Record<PaginationVariant, ButtonVariantMap> = {
  custom: { default: 'outline', ellipsis: 'outline', current: 'solid' },
};

export const PaginationRoot = React.forwardRef<HTMLDivElement, PaginationRootProps>(
  function PaginationRoot(props, ref) {
    const { size = 'md', variant = 'custom', ...rest } = props;
    return (
      <RootPropsProvider value={{ size, variantMap: variantMap[variant] }}>
        <ChakraPagination.Root ref={ref} type="button" {...rest} />
      </RootPropsProvider>
    );
  },
);

export const PaginationEllipsis = React.forwardRef<HTMLDivElement, ChakraPagination.EllipsisProps>(
  function PaginationEllipsis(props, ref) {
    const { size, variantMap } = useRootProps();
    return (
      <ChakraPagination.Ellipsis ref={ref} {...props} asChild>
        <Button
          as="span"
          variant={variantMap.ellipsis}
          size={size}
          borderRadius="8px"
          bg="white"
          borderColor="light"
          px="2px"
          minW="30px"
          h="30px"
        >
          <HiMiniEllipsisHorizontal />
        </Button>
      </ChakraPagination.Ellipsis>
    );
  },
);

export const PaginationItem = React.forwardRef<HTMLButtonElement, ChakraPagination.ItemProps>(
  function PaginationItem(props, ref) {
    const { page } = usePaginationContext();
    const { size, variantMap } = useRootProps();

    const current = page === props.value;
    const variant = current ? variantMap.current : variantMap.default;

    return (
      <ChakraPagination.Item ref={ref} {...props} asChild>
        <Button
          variant={variant}
          size={size}
          borderRadius="8px"
          minW="30px"
          h="30px"
          fontSize="14px"
          px="2px"
          fontWeight="400"
          color={current ? 'white' : 'dark'}
          bg={current ? 'main' : 'white'}
          border="1px solid"
          borderColor={current ? 'main' : 'light'}
          _hover={{ bg: current ? 'blue.600' : 'blue.50' }}
        >
          {props.value}
        </Button>
      </ChakraPagination.Item>
    );
  },
);

export const PaginationPrevTrigger = React.forwardRef<HTMLButtonElement, ChakraPagination.PrevTriggerProps>(
  function PaginationPrevTrigger(props, ref) {
    return (
      <ChakraPagination.PrevTrigger ref={ref} asChild {...props}>
        <IconButton
          aria-label="Previous"
          bg="white"
          variant="outline"
          borderRadius="8px"
          minW="30px"
          h="30px"
          px="2px"
          color="main"
          borderColor="light"
          _hover={{ bg: 'blue.50' }}
        >
          <HiChevronLeft style={{ width: '14px', height: '14px' }} />
        </IconButton>
      </ChakraPagination.PrevTrigger>
    );
  },
);

export const PaginationNextTrigger = React.forwardRef<HTMLButtonElement, ChakraPagination.NextTriggerProps>(
  function PaginationNextTrigger(props, ref) {
    return (
      <ChakraPagination.NextTrigger ref={ref} asChild {...props}>
        <IconButton
          aria-label="Next"
          variant="outline"
          bg="white"
          borderRadius="8px"
          minW="30px"
          h="30px"
          px="2px"
          color="main"
          borderColor="light"
          _hover={{ bg: 'blue.50' }}
        >
          <HiChevronRight style={{ width: '14px', height: '14px' }} />
        </IconButton>
      </ChakraPagination.NextTrigger>
    );
  },
);

export const PaginationItems = (props: React.HTMLAttributes<HTMLElement>) => {
  return (
    <ChakraPagination.Context>
      {({ pages }) =>
        pages.map((page, index) => {
          return page.type === 'ellipsis' ? (
            <PaginationEllipsis key={index} index={index} {...props} />
          ) : (
            <PaginationItem key={index} type="page" value={page.value} {...props} />
          );
        })
      }
    </ChakraPagination.Context>
  );
};
