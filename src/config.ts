import chainConfig from '@config/config.json';
import { Chain, NativeContracts, Token } from '@src/types';

interface ChainConfig {
  nativeChain: Chain;
  nativeContracts: NativeContracts;
  chains: Chain[];
  tokens: Token[];
}

let config: ChainConfig;
try {
  config = process.env.NEXT_PUBLIC_CONFIG ? JSON.parse(process.env.NEXT_PUBLIC_CONFIG) : chainConfig;
} catch (error) {
  console.error('Error parsing NEXT_PUBLIC_CONFIG:', error);
  config = chainConfig;
}

const chainMap: { [identifier: string]: Chain } = {};
for (const chain of config.chains) {
  chainMap[chain.identifier] = chain;
}

const chainByIdMap: { [id: string]: Chain } = {};
for (const chain of config.chains) {
  chainByIdMap[chain.chainId] = chain;
}

const tokenMap: { [identifier: string]: Token } = {};
for (const token of config.tokens) {
  tokenMap[token.identifier] = token;
}

const tokenByChainAndAddressMap: { [address: string]: Token | undefined } = {};
for (const token of config.tokens) {
  for (const [chainIdentifier, address] of Object.entries(token.chains)) {
    tokenByChainAndAddressMap[`${chainIdentifier}-${address}`] = token;
  }
}

export function getNativeChain(): Chain {
  return config.nativeChain;
}

export function getNativeContracts(): NativeContracts {
  return config.nativeContracts;
}

export function getSupportedChains(): Chain[] {
  return config.chains;
}

export function getSupportedTokens(): Token[] {
  return config.tokens;
}

export function getChain(identifier: string): Chain {
  return chainMap[identifier];
}

export function getChainById(chainId: number): Chain {
  return (
    chainByIdMap[chainId] ??
    (() => {
      throw new Error(`Chain not found for ID: ${chainId}`);
    })()
  );
}

export function getToken(identifier: string): Token {
  return tokenMap[identifier];
}

export function getTokenByChainIdentifierAndAddress(chainIdentifier: string, address: string): Token | undefined {
  return tokenByChainAndAddressMap[`${chainIdentifier}-${address}`];
}

export function getChainParams(chain: Chain) {
  return {
    chainId: `0x${chain.chainId.toString(16)}`,
    chainName: chain.name,
    nativeCurrency: chain.nativeCurrency,
    rpcUrls: [chain.rpc],
    blockExplorerUrls: [chain.explorer],
  };
}

export const getShortAddress = (address: string, start = 6, endCount = 4) =>
  `${address?.slice(0, start)}...${address?.slice(address?.length - (endCount ?? start + 1))}`;
