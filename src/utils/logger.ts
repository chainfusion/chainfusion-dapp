import loggerConfig from '@/utils/config';

interface Logger {
  log: (description: string, ...args: unknown[]) => void;
  error: (description: string, ...args: unknown[]) => void;
  warn: (description: string, ...args: unknown[]) => void;
  info: (description: string, ...args: unknown[]) => void;
}

const logger: Logger = {
  log: (description: string, ...args: unknown[]): void => {
    if (loggerConfig.loggingEnabled) {
      console.log(`%c[LOG] ${description}`, 'background: #222; color: #bada55', ...args);
    }
  },
  error: (description: string, ...args: unknown[]): void => {
    if (loggerConfig.loggingEnabled) {
      console.error(`%c[ERROR] ${description}`, 'background: #ff0000; color: #fff', ...args);
    }
  },
  warn: (description: string, ...args: unknown[]): void => {
    if (loggerConfig.loggingEnabled) {
      console.warn(`%c[WARN] ${description}`, 'background: #ffcc00; color: #000', ...args);
    }
  },
  info: (description: string, ...args: unknown[]): void => {
    if (loggerConfig.loggingEnabled) {
      console.info(`%c[INFO] ${description}`, 'background: #00f; color: #fff', ...args);
    }
  },
};

export default logger;
