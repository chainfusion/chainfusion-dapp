import { Connector } from 'wagmi';

export const socialLinks = [
  { href: 'https://x.com/chain_fusion', label: 'Twitter' },
  { href: 'https://t.me/chainfusion', label: 'Telegram' },
  { href: '#', label: 'Medium' },
  { href: 'https://www.linkedin.com/company/chainfusion', label: 'LinkedIn' },
];

export const navLinks = [
  { key: 'bridge', href: '/', label: 'Bridge' },
  { key: 'liquidity', href: '/liquidity', label: 'Liquidity' },
  { key: 'staking', href: '/staking', label: 'Staking' },
  { key: 'slashing', href: '/slashing', label: 'Slashing' },
];

export const getWalletInfo = (connector?: Connector | null) => {
  if (!connector) return { name: 'Unknown', logo: '/img/wallet/default.svg' };

  const connectorId = connector.id as keyof typeof wallets;

  const wallets: Record<string, { name: string; logo: string }> = {
    metaMask: { name: 'MetaMask', logo: '/img/wallet/metamask.svg' },
    coinbaseWallet: { name: 'Coinbase Wallet', logo: '/img/wallet/coinbase.svg' },
    walletConnect: { name: 'WalletConnect', logo: '/img/wallet/walletconnect.svg' },
    injected: { name: 'Injected Wallet', logo: '/img/wallet/default.svg' },
  };

  return wallets[connectorId] || { name: 'Unknown', logo: '/img/wallet/default.svg' };
};

export enum APP_MODULES {
  ANALYTICS = 'analytics',
  BRIDGE = 'bridge',
  LIQUIDITY = 'liquidity',
  SLASHING = 'slashing',
  STAKING = 'staking',
}

export interface LoggerConfig {
  loggingEnabled: boolean;
}

const loggerConfig: LoggerConfig = {
  loggingEnabled: true,
};

export default loggerConfig;
