import { defineRecipe } from '@chakra-ui/react';

export const button = defineRecipe({
  base: {
    fontFamily: 'body',
    display: 'inline-flex',
    alignItems: 'center',
    justifyContent: 'center',
    lineHeight: '28px',
    borderRadius: '40px',
    px: '50px',
    py: '0',
    height: '62px',
    transition: 'all ease-out 0.3s',
    _disabled: {
      opacity: 0.4,
      cursor: 'not-allowed',
    },
  },
  variants: {
    variant: {
      solid: {
        bg: 'main',
        color: 'white',
        border: '1px solid var(--chakra-colors-main)',
        borderRadius: '50px',
        borderColor: 'main',
        _hover: {
          bg: 'hover',
        },
      },
      light: {
        bg: '#e8f1fb',
        color: 'main',
        border: 0,
        borderRadius: '50px',
        _hover: {
          bg: '#d3e8ff',
        },
        _disabled: {
          opacity: 0.8,
        },
      },
      outline: {
        border: '1px solid var(--chakra-colors-main)',
        bgColor: 'transparent',
        borderColor: 'main',
        borderRadius: '50px !important',
        color: 'main',
        _hover: {
          bg: 'main',
          color: 'white',
          borderColor: 'main',
        },
      },
      ghost: {
        bg: 'transparent',
        p: 0,
        border: 0,
        h: 'auto',
        color: 'main',
        _hover: {
          bg: 'lightBlue',
        },
      },
    },
    size: {
      sm: {
        padding: '0 20px',
        h: '40px',
        fontSize: '14px !important',
      },
      medium: {
        padding: '0 20px',
        h: '45px',
        fontSize: '14px !important',
      },
      lg: {
        padding: '0 50px',
        h: '58px',
        w: '100%',
        fontSize: '16px',
      },
    },
  },
  defaultVariants: {
    variant: 'solid',
    size: 'lg',
  },
});
