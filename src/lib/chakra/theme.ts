import { createSystem, defaultConfig, defineTokens } from '@chakra-ui/react';
import { semanticShadows } from './semantic-tokens/shadows';
import { tooltipSlotRecipe } from '@/lib/chakra/recipes/tooltip';
import { progressCircleSlotRecipe } from '@/lib/chakra/recipes/progress-circle';
import { dialogSlotRecipe } from '@/lib/chakra/recipes/dialog';

const tokens = defineTokens({
  colors: {
    main: { value: '#0d7ee7' },
    hover: { value: '#188ffe' },
    active: { value: '#0064c9' },
    light: { value: '#e4edf8' },
    lightBlue: { value: '#c5e0fd' },
    white: { value: '#ffffff' },
    silver: { value: '#a0afb9' },
    dark: { value: '#06192F' },
    darkLine: { value: '#40536B' },
    grayTwo: { value: '#8694a5' },
    grayThree: { value: '#52657A' },
    success: { value: '#29c195' },
    info: { value: '#0d7ee7' },
    warning: { value: '#ffae00' },
    error: { value: '#fc574d' },
    successLight: { value: '#dff6ef' },
    infoLight: { value: '#e7f2fd' },
    warningLight: { value: '#fff7e6' },
    errorLight: { value: '#fdf4f6' },
    light1: { value: '#e6eef7' },
    light2: { value: '#f1f3f5' },
    light3: { value: '#f5f8fc' },
    light4: { value: '#e4edf8' },
    light5: { value: '#dff6ef' },
    light6: { value: '#dee2e6' },
    gray: {
      50: { value: '#fafafa' },
      100: { value: '#f4f4f5' },
      200: { value: '#e4e4e7' },
      300: { value: '#d4d4d8' },
      400: { value: '#a1a1aa' },
      500: { value: '#71717a' },
      600: { value: '#52525b' },
      700: { value: '#3f3f46' },
      800: { value: '#27272a' },
      900: { value: '#18181b' },
      950: { value: '#111111' },
    },
  },
  fonts: {
    heading: { value: `'Inter', sans-serif` },
    body: { value: `'Inter', sans-serif` },
    mono: { value: `'Noto Sans Mono', monospace` },
  },
  radii: {
    base: { value: '8px' },
    md: { value: '12px' },
    lg: { value: '50px' },
    full: { value: '9999px' },
  },
  space: {
    px: { value: '1px' },
    0: { value: '0px' },
    1: { value: '4px' },
    2: { value: '8px' },
    3: { value: '12px' },
    4: { value: '16px' },
    5: { value: '20px' },
    6: { value: '24px' },
  },
  sizes: {
    container: {
      sm: { value: '540px' },
      md: { value: '720px' },
      lg: { value: '960px' },
      xl: { value: '1140px' },
      full: { value: '100%' },
    },
  },
});

export const system = createSystem(defaultConfig, {
  globalCss: {
    '*': {
      margin: 0,
      padding: 0,
      boxSizing: 'border-box',
    },
    'html, body': {
      width: '100%',
      height: '100%',
      margin: 0,
      padding: 0,
      boxSizing: 'border-box',
      fontFamily: 'var(--chakra-fonts-body)',
      bgColor: {
        base: 'white',
        _dark: '#06192F',
      },
      // backgroundImage: "url('/img/bg.svg')",

      backgroundImage: {
        base: "url('/img/bg.svg')",
        _dark: "url('/img/bg-dark.svg')",
      },
      backgroundAttachment: 'fixed',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      overflowX: 'hidden',
    },
    a: {
      textDecoration: 'none',
      transition: 'all ease-out 0.1s',
    },
    img: {
      display: 'inline-flex',
    },
    'input, textarea': {
      borderRadius: 'base',
      border: '1px solid',
      borderColor: 'light',
      transition: 'all ease-out 0.3s',
      '&:focus, &:hover': {
        borderColor: 'lightBlue',
      },
      '&::placeholder': {
        color: 'silver',
      },
    },
    '.custom-select-trigger button': {
      border: 0,
      h: '50px',
    },
  },
  theme: {
    tokens,
    slotRecipes: {
      tooltip: tooltipSlotRecipe,
      progressCircle: progressCircleSlotRecipe,
      dialog: dialogSlotRecipe,
    },
    semanticTokens: {
      shadows: semanticShadows,
    },
    recipes: {
      container: {
        base: {
          px: '15px',
          mx: 'auto',
          width: '100%',
          maxW: 'container.xl',
        },
      },
    },
  },
});
