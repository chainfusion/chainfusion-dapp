export const walletConfig: Record<string, { iconSrc: string }> = {
  metaMask: { iconSrc: '/img/wallet/metamask.svg' },
  coinbaseWallet: { iconSrc: '/img/wallet/coinbase.svg' },
  walletConnect: { iconSrc: '/img/wallet/walletconnect.svg' },
};
