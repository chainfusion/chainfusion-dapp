import { Chain, configureChains, createConfig } from 'wagmi';
import { MetaMaskConnector } from 'wagmi/connectors/metaMask';
import { WalletConnectConnector } from 'wagmi/connectors/walletConnect';
import { CoinbaseWalletConnector } from 'wagmi/connectors/coinbaseWallet';
import { alchemyProvider } from 'wagmi/providers/alchemy';
import { publicProvider } from 'wagmi/providers/public';

import { getSupportedChains } from '@src/config';

export const supportedChains: Chain[] = getSupportedChains().map((chain) => ({
  id: chain.chainId,
  name: chain.name,
  network: chain.name.toLowerCase(),
  rpcUrls: {
    default: { http: [chain.rpc] },
    public: { http: [chain.rpc] },
  },
  blockExplorers: { default: { name: 'Explorer', url: chain.explorer } },
  nativeCurrency: chain.nativeCurrency || { name: 'CFN', symbol: 'CFN', decimals: 18 },
}));

const projectId = '08520a645084b69cc4b98e5b00268b1b';

const { chains, publicClient, webSocketPublicClient } = configureChains(supportedChains, [
  alchemyProvider({ apiKey: 'ocNInY9-gv5WPZLDeaFd5wvohw3m5siy' }),
  publicProvider(),
]);

export const wagmiConfig = createConfig({
  autoConnect: true,
  connectors: [
    new MetaMaskConnector({ chains }),
    new WalletConnectConnector({
      chains,
      options: {
        projectId: projectId,
        qrModalOptions: { themeVariables: { '--wcm-z-index': '9999' } },
      },
    }),
    new CoinbaseWalletConnector({
      chains,
      options: {
        appName: 'ChainFusion',
      },
    }),
  ],
  publicClient,
  webSocketPublicClient,
});
