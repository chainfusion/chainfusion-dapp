import React from 'react';
import { MenuRoot, MenuTrigger, MenuContent, MenuItem } from '@/components/ui/menu';
import { Button, Flex, Icon, Text, Circle } from '@chakra-ui/react';
import { ClipboardRoot, ClipboardLink } from '@/components/ui/clipboard';

import { WalletIcon, External, Switch, Logout } from '@components/Icons';
import { useAccount, useDisconnect, useNetwork } from 'wagmi';
import { useColorMode } from '@/components/ui/color-mode';
import { getShortAddress } from '@/config';
import { useChainContext } from '@/context/ChainContext';

import config from '@config/config.json';

const getExplorerUrl = (chainId: number) => {
  const chain = [...config.chains, config.nativeChain].find((c) => c.chainId === chainId);
  return chain?.explorer || 'https://etherscan.io';
};

const menuItemStyles = {
  borderRadius: '16px',
  _hover: { bg: { base: 'light', _dark: 'darkLine' } },
  cursor: 'pointer',
};

export const WalletMenu = () => {
  const { chain } = useNetwork();
  const { disconnect } = useDisconnect();
  const { address, isConnected } = useAccount();
  const { toggleColorMode, colorMode } = useColorMode();
  const { showConnectWalletDialog } = useChainContext();

  const openExplorer = () => {
    if (!address) return;

    const baseUrl = getExplorerUrl(chain?.id || 1);
    window.open(`${baseUrl}/address/${address}`, '_blank');
  };

  return isConnected && address ? (
    <MenuRoot>
      <MenuTrigger asChild>
        <Button
          outline="none"
          borderRadius="full"
          p="0 18px 0 8px"
          bg={{ base: 'light3', _dark: '#192B43' }}
          border="1px solid"
          borderColor="main"
          color={{ base: 'main', _dark: 'white' }}
          _hover={{
            bg: {
              base: '#e2effb',
              _dark: 'hover',
            },
          }}
          boxShadow="0px 4px 8px rgba(5, 59, 121, 0.08)"
          h="48px"
          pos="relative"
          zIndex={2222}
        >
          <Flex align="center" gap={2}>
            <Circle bgColor="main" color="white" w={8} h={8}>
              <Icon as={WalletIcon} boxSize="20px" />
            </Circle>
            <Text fontSize="14px">{getShortAddress(address)}</Text>
          </Flex>
        </Button>
      </MenuTrigger>

      {isConnected && (
        <MenuContent
          w="100%"
          maxW="180px"
          minW="166px"
          bg={{ base: 'white', _dark: '#06192F' }}
          color={{ base: 'dark', _dark: 'white' }}
          border="1px solid"
          borderColor={{ base: 'light', _dark: 'darkLine' }}
          borderRadius="16px"
          boxShadow="0px 5px 10px rgba(5, 59, 121, 0.08)"
          p={2}
          pt="36px"
          top="-40px"
          pos="relative"
        >
          <MenuItem value="copyAddress" closeOnSelect={false} {...menuItemStyles}>
            <ClipboardRoot outline="none" w="100%" mb={0} p={0} value={address}>
              <ClipboardLink w="100%" outline="none" cursor="pointer" display="flex" mb={0} p={0} />
            </ClipboardRoot>
          </MenuItem>
          <MenuItem value="explorer" onClick={openExplorer} {...menuItemStyles}>
            <Icon as={External} />
            <Text>Go to Explorer</Text>
          </MenuItem>
          <MenuItem value="darkMode" onClick={toggleColorMode} {...menuItemStyles}>
            <Icon as={Switch} />
            <Text>{colorMode === 'light' ? 'Dark Mode' : 'Light mode'}</Text>
          </MenuItem>
          <MenuItem
            value="disconnect"
            onClick={() => disconnect()}
            color="red.400"
            borderRadius="16px"
            _hover={{ bg: 'red.600', color: 'white' }}
            cursor="pointer"
          >
            <Icon as={Logout} />
            <Text>Disconnect</Text>
          </MenuItem>
        </MenuContent>
      )}
    </MenuRoot>
  ) : (
    <Button
      onClick={() => showConnectWalletDialog()}
      borderRadius="full"
      px={5}
      bg="main"
      color="white"
      _hover={{ bg: 'hover' }}
      h="48px"
    >
      <Icon as={WalletIcon} boxSize="20px" />
      <Text as="span">Connect Wallet</Text>
    </Button>
  );
};
