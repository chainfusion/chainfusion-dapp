'use client';

import InputCFNModal from '@components/Modals/InputCFNModal';
import { Container, Flex, Image, Grid, Box, Text, VStack } from '@chakra-ui/react';
import { ReactElement, useCallback, useEffect, useState } from 'react';
import { StakingHeader, StakingItem, SkeletonStakingItem } from '@components/Staking/StakingTable';
import { useChainContext } from '@src/context/ChainContext';
import { getNativeChain } from '@src/config';
import { StakingInfo, ValidatorInfo } from '@src/types';
import { useStakingStore, defaultStakingInfo } from '@store/staking';
import { BigNumber, utils } from 'ethers';
import dayjs from 'dayjs';
import { showAnnounceSuccessToast, showStakeSuccessToast, showWithdrawSuccessToast } from '@components/Alerts/Staking';
import { IoArrowUpCircleOutline, IoArrowDownCircleOutline } from 'react-icons/io5';
import { Button } from '@/components/ui/button';
import { TbMessageDown } from 'react-icons/tb';
import { PiShuffle, PiWallet } from 'react-icons/pi';
import { useAccount } from 'wagmi';

const Staking = () => {
  const [showIncreaseStakeModal, setShowIncreaseStakeModal] = useState(false);
  const [showAnnounceWithdrawalModal, setShowAnnounceWithdrawalModal] = useState(false);

  const [balance, setBalance] = useState<BigNumber>(BigNumber.from(0));
  const [stakingPending, setStakingPending] = useState(false);
  const [announcePending, setAnnouncePending] = useState(false);
  const [withdrawalPending, setWithdrawalPending] = useState(false);

  const {
    validators,
    validatorsLoading,
    setValidators,

    stakingInfo,
    stakingInfoLoading,
    setStakingInfo,
    setStakingInfoLoading,
  } = useStakingStore();

  const { isConnected: isActive } = useAccount();
  const { nativeContainer, switchNetwork, showConnectWalletDialog } = useChainContext();
  const nativeChain = getNativeChain();

  const nativeConnected = nativeContainer?.connected ?? false;

  const getValidatorIcon = () => {
    if (stakingInfo.status === 1) {
      return 'validator';
    }

    return 'not-validator';
  };

  const getValidatorBadgeClass = () => {
    if (stakingInfo.status === 1) {
      return 'active-validator';
    }

    return 'inactive-validator';
  };

  const getValidatorStatus = () => {
    if (stakingInfoLoading) {
      return 'Loading';
    }

    switch (stakingInfo.status) {
      case 0:
        return 'Not a Validator';
      case 1:
        return 'Validator';
      case 2:
        return 'Slashed';
    }

    return 'Unknown';
  };

  const loadValidators = useCallback(async () => {
    if (nativeContainer === undefined) {
      return [];
    }

    const { staking } = nativeContainer;

    const validatorsCount = await staking.getValidatorsCount();
    const validators: ValidatorInfo[] = await staking.listValidators(0, validatorsCount);

    const sortedValidators = [...validators];
    sortedValidators.sort((a, b) => {
      if (a.stake.eq(b.stake)) {
        return a.validator.localeCompare(b.validator);
      }

      return a.stake.lt(b.stake) ? 1 : -1;
    });

    return sortedValidators;
  }, [nativeContainer]);

  const loadStakingInfo = useCallback(async () => {
    if (nativeContainer === undefined) {
      return undefined;
    }

    const { staking, account } = nativeContainer;
    const stake = await staking.stakes(account);
    if (stake.stake.eq(0)) {
      return undefined;
    }

    const withdrawalPeriod = await staking.withdrawalPeriod();

    const withdrawalInfo = await staking.withdrawalAnnouncements(account);

    const result: StakingInfo = {
      ...stake,
      withdrawalAmount: withdrawalInfo.amount,
      withdrawalTime: withdrawalInfo.time.add(withdrawalPeriod),
    };

    return result;
  }, [nativeContainer]);

  const loadBalance = useCallback(async () => {
    if (nativeContainer === undefined) {
      return undefined;
    }

    const { provider, account } = nativeContainer;
    return await provider.getBalance(account);
  }, [nativeContainer]);

  useEffect(() => {
    if (nativeContainer === undefined) {
      return;
    }

    let pending = true;

    loadValidators().then((validators) => {
      if (pending) {
        setValidators(validators);
      }
    });

    return () => {
      pending = false;
    };
  }, [nativeContainer, stakingPending, withdrawalPending, setValidators, loadValidators]);

  useEffect(() => {
    if (nativeContainer === undefined) {
      return;
    }

    let pending = true;

    setBalance(BigNumber.from(0));
    setStakingInfoLoading();

    loadStakingInfo().then((stakingInfo) => {
      if (pending) {
        if (stakingInfo !== undefined) {
          setStakingInfo(stakingInfo);
        } else {
          setStakingInfo(defaultStakingInfo);
        }
      }
    });

    loadBalance().then((balance) => {
      if (pending) {
        if (balance === undefined) {
          setBalance(BigNumber.from(0));
        } else {
          setBalance(balance);
        }
      }
    });

    return () => {
      pending = false;
    };
  }, [
    nativeContainer,
    stakingPending,
    announcePending,
    withdrawalPending,
    setStakingInfo,
    setStakingInfoLoading,
    loadStakingInfo,
    loadBalance,
  ]);

  const buttons: ReactElement[] = [];

  if (nativeConnected) {
    if (!stakingInfoLoading && stakingInfo.status !== 2) {
      buttons.push(
        <Button
          key="increase-stake"
          size="medium"
          disabled={stakingPending}
          loading={stakingPending}
          loadingText="Increase Stake ..."
          borderRadius="40px"
          w="full"
          onClick={() => setShowIncreaseStakeModal(true)}
        >
          <IoArrowUpCircleOutline />
          Increase Stake
        </Button>,
      );
    }

    if (BigNumber.isBigNumber(stakingInfo.stake) && stakingInfo.stake.gt(0)) {
      buttons.push(
        <Button
          key="announce-withdrawal"
          variant="light"
          size="medium"
          disabled={announcePending}
          loading={announcePending}
          loadingText="Announce Withdrawal ..."
          borderRadius="40px"
          w="full"
          onClick={() => setShowAnnounceWithdrawalModal(true)}
        >
          <TbMessageDown />
          Announce Withdrawal
        </Button>,
      );
    }

    if (
      BigNumber.isBigNumber(stakingInfo.withdrawalAmount) &&
      stakingInfo.withdrawalAmount.gt(0) &&
      stakingInfo.withdrawalTime.lt(Math.round(new Date().getTime() / 1000))
    ) {
      buttons.push(
        <Button
          key="withdraw-stake"
          size="medium"
          variant="light"
          disabled={withdrawalPending}
          loading={withdrawalPending}
          loadingText="Withdraw Stake ..."
          borderRadius="40px"
          w="full"
          onClick={async () => {
            if (nativeContainer === undefined) {
              return;
            }

            const { staking } = nativeContainer;
            setWithdrawalPending(true);
            await (await staking.withdraw()).wait();
            setWithdrawalPending(false);

            showWithdrawSuccessToast();
          }}
        >
          <IoArrowDownCircleOutline />
          Withdraw Stake
        </Button>,
      );
    }
  } else if (isActive) {
    buttons.push(
      <Button
        size="medium"
        key="switch-network"
        loadingText="Switch Network ..."
        borderRadius="40px"
        w="full"
        onClick={() => switchNetwork(nativeChain)}
      >
        <PiShuffle />
        Switch Network
      </Button>,
    );
  } else {
    buttons.push(
      <Button
        size="medium"
        key="connect-wallet"
        borderRadius="40px"
        w="full"
        onClick={() => showConnectWalletDialog(nativeChain)}
      >
        <PiWallet />
        Connect Wallet
      </Button>,
    );
  }

  return (
    <>
      <Box as="section" pt={{ base: '100px', lg: '3rem' }} pb="1.5rem">
        <Container>
          <Grid templateColumns={{ base: 'minmax(0, 1fr)', lg: '350px 1fr' }} gap="30px">
            <Box>
              <Box
                p="25px"
                bg={{ base: 'white', _dark: '#192B43' }}
                border="1px solid"
                borderColor={{ base: 'light', _dark: 'darkLine' }}
                borderRadius="15px"
                boxShadow="0px 10px 20px rgba(75, 105, 139, 0.03)"
                mb="4"
              >
                <Image
                  src={`/img/avatar/${getValidatorIcon()}.svg`}
                  alt="Validator"
                  w="92px"
                  h="92px"
                  mb="12px"
                  display="block"
                  mx="auto"
                  bgColor={{ base: '#F5F8FC', _dark: '#0F223A' }}
                  borderRadius="full"
                />

                <Flex justifyContent="center">
                  <Text
                    px="20px"
                    py="7px"
                    bg={getValidatorBadgeClass() === 'active-validator' ? 'light5' : 'light3'}
                    borderRadius="100px"
                    fontWeight="600"
                    fontSize="0.75rem"
                    color={getValidatorBadgeClass() === 'active-validator' ? 'success' : 'gray.500'}
                    mb={0}
                  >
                    {getValidatorStatus()}
                  </Text>
                </Flex>

                <Box w="full" px="15px">
                  <Box
                    border="1px solid"
                    borderColor={{ base: 'light', _dark: 'darkLine' }}
                    borderRadius="15px"
                    p="15px 0 12px"
                    my="4"
                  >
                    <Text
                      maxW="100px"
                      fontWeight="400"
                      fontSize="0.8125rem"
                      lineHeight="16px"
                      textAlign="center"
                      color="gray.500"
                      mx="auto"
                      mb="5px"
                    >
                      Current Stake
                    </Text>
                    <Text fontWeight="600" fontSize="1.5rem" lineHeight="29px" textAlign="center" mb={0} color="main">
                      {/* eslint-disable-next-line max-len */}
                      {`${utils.formatUnits(stakingInfo.stake, nativeChain.nativeCurrency.decimals)} ${nativeChain.nativeCurrency.symbol}`}
                    </Text>
                  </Box>
                </Box>

                {BigNumber.isBigNumber(stakingInfo.withdrawalTime) &&
                  !stakingInfo.withdrawalTime.eq(0) &&
                  stakingInfo.withdrawalAmount.gt(0) && (
                    <Flex direction={{ base: 'column', sm: 'row' }} gap="2">
                      <Box w={{ base: 'full', sm: '50%' }} pr={{ base: 0, sm: 2 }}>
                        <Box
                          display="flex"
                          flexDirection="column"
                          justifyContent="center"
                          border="1px solid"
                          borderColor="light"
                          borderRadius="15px"
                          h="85px"
                          pt="2px"
                          mb="7px"
                        >
                          <Text
                            maxW="100px"
                            fontWeight="400"
                            fontSize="0.8125rem"
                            lineHeight="16px"
                            textAlign="center"
                            color="gray.500"
                            mx="auto"
                            mb="5px"
                          >
                            Announced Withdrawal
                          </Text>
                          <Text
                            fontWeight="600"
                            fontSize="1.5rem"
                            lineHeight="29px"
                            textAlign="center"
                            color="gray.900"
                          >
                            {`${utils.formatUnits(stakingInfo.withdrawalAmount, nativeChain.nativeCurrency.decimals)}`}
                          </Text>
                        </Box>
                      </Box>

                      <Box w={{ base: 'full', sm: '50%' }} pl={{ base: 0, sm: 2 }}>
                        <Box
                          display="flex"
                          flexDirection="column"
                          alignItems="center"
                          justifyContent="center"
                          border="1px solid"
                          borderColor="light"
                          borderRadius="15px"
                          h="85px"
                          bg="gray.50"
                        >
                          <Text fontWeight="400" fontSize="0.8125rem" color="gray.500" mb="5px">
                            Withdrawal Time
                          </Text>
                          <Text fontWeight="600" fontSize="1rem" color="gray.900">
                            {dayjs.unix(stakingInfo.withdrawalTime.toNumber()).format('L LT')}
                          </Text>
                        </Box>
                      </Box>
                    </Flex>
                  )}
                <VStack gap="12px">{buttons}</VStack>
              </Box>
            </Box>

            <Box>
              <StakingHeader />
              {validatorsLoading ? (
                <Box>
                  <SkeletonStakingItem />
                  <SkeletonStakingItem />
                  <SkeletonStakingItem />
                  <SkeletonStakingItem />
                  <SkeletonStakingItem />
                </Box>
              ) : (
                <Box>
                  {validators.map((data, i) => {
                    const rank = i + 1;

                    return <StakingItem key={data.validator} rank={rank} data={data} />;
                  })}
                </Box>
              )}
            </Box>
          </Grid>
        </Container>
      </Box>

      <InputCFNModal
        show={showIncreaseStakeModal}
        decimals={nativeChain.nativeCurrency.decimals}
        maxValue={balance}
        maxValueText="Available to stake"
        title="Increase Stake"
        buttonText="Stake"
        submit={async (amount) => {
          if (nativeContainer === undefined) {
            return;
          }

          const { staking } = nativeContainer;
          setStakingPending(true);
          await (await staking.stake({ value: amount })).wait();
          setStakingPending(false);

          showStakeSuccessToast();
        }}
        close={() => setShowIncreaseStakeModal(false)}
      />

      <InputCFNModal
        show={showAnnounceWithdrawalModal}
        decimals={nativeChain.nativeCurrency.decimals}
        maxValue={stakingInfo.stake}
        maxValueText="Available to announce"
        title="Announce Withdrawal"
        buttonText="Announce"
        submit={async (amount) => {
          if (nativeContainer === undefined) {
            return;
          }

          const { staking } = nativeContainer;
          setAnnouncePending(true);
          await (await staking.announceWithdrawal(amount)).wait();
          setAnnouncePending(false);

          showAnnounceSuccessToast();
        }}
        close={() => setShowAnnounceWithdrawalModal(false)}
      />
    </>
  );
};

export default Staking;
