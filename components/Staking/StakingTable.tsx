import { Flex, Box, Text, Link } from '@chakra-ui/react';
import { SkeletonCircle, SkeletonText } from '@/components/ui/skeleton';
import { getNativeChain } from '@src/config';
import { ValidatorInfo } from '@src/types';
import { utils } from 'ethers';
import { ClipboardIconButton, ClipboardRoot } from '@/components/ui/clipboard';
import { Tooltip } from '@/components/ui/tooltip';
import { FiCopy } from 'react-icons/fi';

export const StakingHeader = () => {
  return (
    <Flex
      p="10px 20px"
      bg={{ base: 'white', _dark: '#192B43' }}
      border="1px solid"
      borderColor={{ base: 'light', _dark: 'darkLine' }}
      borderRadius="15px"
      boxShadow="0px 10px 20px rgba(75, 105, 139, 0.03)"
      fontWeight="700"
      fontSize="0.8125rem"
      color={{ base: 'dark', _dark: 'light' }}
      alignItems="center"
    >
      <Box
        minW="45px"
        textAlign="center"
        pr="2"
        borderRight="1px solid"
        borderColor={{ base: 'light', _dark: 'darkLine' }}
      >
        Rank
      </Box>
      <Box flex="1" pl="4" borderRight="1px solid" borderColor={{ base: 'light', _dark: 'darkLine' }}>
        Validator
      </Box>
      <Box minW="130px" textAlign="center" pl="5" pr="4">
        Stake
      </Box>
    </Flex>
  );
};

export interface StakingItemProps {
  rank: number;
  data: ValidatorInfo;
}

export const StakingItem = ({ rank, data }: StakingItemProps) => {
  const nativeChain = getNativeChain();

  const getAddressUrl = (address: string) => {
    return new URL(`/address/${address}`, nativeChain.explorer).href;
  };

  const getShortValidator = (tx: string) => {
    const visibleCharacters = 16;
    return `${tx.substring(0, 2 + visibleCharacters)}...${tx.substring(tx.length - visibleCharacters)}`;
  };

  return (
    <Flex
      mt="5px"
      p="10px 20px"
      bg={{ base: 'white', _dark: '#192B43' }}
      border="1px solid"
      borderColor={{ base: 'light', _dark: 'darkLine' }}
      borderRadius="15px"
      boxShadow="0px 10px 20px rgba(75, 105, 139, 0.03)"
      alignItems={{ base: 'flex-start', lg: 'center' }}
      flexDir={{ base: 'column', lg: 'row' }}
    >
      <Box
        minW="45px"
        fontWeight={{ base: 700, lg: 600 }}
        textAlign="center"
        borderRight={{
          base: 0,
          lg: {
            base: '1px solid var(--chakra-colors-light)',
            _dark: '1px solid var(--chakra-colors-dark-line)',
          },
        }}
        pr={{ base: 0, md: 3 }}
        display={{ base: 'flex', lg: 'block' }}
        fontSize="0.8125rem"
      >
        <Text display={{ base: 'block', md: 'none' }} mb={0}>
          Rank:
        </Text>
        &nbsp;
        {rank}
      </Box>

      <Box
        flex="1"
        pl={{ base: 0, md: 4 }}
        borderRight={{
          base: 0,
          lg: {
            base: '1px solid var(--chakra-colors-light)',
            _dark: '1px solid var(--chakra-colors-dark-line)',
          },
        }}
        fontSize="0.8125rem"
        gap="6px"
      >
        <Text display={{ base: 'block', md: 'none' }} fontWeight="600" mb={0}>
          Validator:
        </Text>
        <Flex alignItems="center">
          <Link
            href={getAddressUrl(data.validator)}
            target="_blank"
            rel="noreferrer"
            fontWeight="400"
            fontSize="0.8125rem"
            fontFamily="'Noto Sans Mono', monospace"
            _hover={{ color: 'hoverColor' }}
          >
            <Text display={{ base: 'block', sm: 'none' }} mb={0}>
              {getShortValidator(data.validator)}
            </Text>
            <Text display={{ base: 'none', sm: 'block' }} mb={0}>
              {data.validator}
            </Text>
          </Link>

          <ClipboardRoot value={data.validator}>
            <Tooltip content="Copy to clipboard" showArrow openDelay={50} closeDelay={100}>
              <ClipboardIconButton display="inline-flex" p={0} w="14px" h="14px" icon={<FiCopy />} />
            </Tooltip>
          </ClipboardRoot>
        </Flex>
      </Box>

      <Box
        minW="130px"
        fontWeight="700"
        fontSize="0.8125rem"
        textAlign="center"
        color="main"
        pl={{ base: 0, md: 4 }}
        pr={{ base: 0, md: 6 }}
        display={{ base: 'flex', lg: 'block' }}
      >
        <Text display={{ base: 'block', md: 'none' }} mb={0}>
          Stake:
        </Text>
        &nbsp;
        {utils.formatEther(data.stake)} {nativeChain.nativeCurrency.symbol}
      </Box>
    </Flex>
  );
};

export const SkeletonStakingItem = () => {
  return (
    <Flex
      mt="5px"
      p="10px 20px"
      bg="white"
      border="1px solid"
      borderColor="light"
      borderRadius="15px"
      boxShadow="0px 10px 20px rgba(75, 105, 139, 0.03)"
      alignItems={{ base: 'flex-start', md: 'center' }}
      flexDir={{ base: 'column', md: 'row' }}
      gap={{ base: '10px', md: 0 }}
    >
      <Box
        minW="45px"
        borderRight={{ base: 0, lg: '1px solid var(--chakra-colors-light)' }}
        textAlign="center"
        pr={{ base: 0, md: 3 }}
      >
        <SkeletonCircle variant="shine" size="24px" />
      </Box>

      <Box
        flex="1"
        pl={{ base: 0, md: 4 }}
        borderRight={{ base: 0, lg: '1px solid var(--chakra-colors-light)' }}
        pr="1.5rem"
        w="100%"
      >
        <SkeletonText variant="shine" noOfLines={2} gap="2" />
      </Box>

      <Box minW="130px" pl={{ base: 0, md: 4 }} pr={{ base: 0, md: 6 }} display={{ base: 'none', md: 'block' }}>
        <SkeletonText variant="shine" noOfLines={2} gap="2" />
      </Box>
    </Flex>
  );
};
