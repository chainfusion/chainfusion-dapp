import { toaster } from '@/components/ui/toaster';

export const showStakeSuccessToast = () => {
  toaster.create({
    title: 'Success',
    description: 'You have successfully increased your stake',
    type: 'success',
  });
};

export const showAnnounceSuccessToast = () => {
  toaster.create({
    title: 'Success',
    description: 'You have successfully announced withdrawal',
    type: 'success',
  });
};

export const showWithdrawSuccessToast = () => {
  toaster.create({
    title: 'Success',
    description: 'You have successfully withdrawn stake',
    type: 'success',
  });
};
