'use client';

import { BridgeTransfer } from '@src/types';
import { BigNumber } from 'ethers';
import { getAddressLink, trimDecimals } from '@src/utils';
import { utils } from 'ethers';
import { Box, Flex, Grid, Text, Image, Icon, Link, Skeleton, Accordion, useMediaQuery } from '@chakra-ui/react';
import { FiCopy } from 'react-icons/fi';
import { FaAngleDown } from 'react-icons/fa6';
import { LiaLongArrowAltRightSolid } from 'react-icons/lia';
import { BsRepeat } from 'react-icons/bs';
import { Tooltip } from '@/components/ui/tooltip';
import { ClipboardRoot, ClipboardIconButton } from '@/components/ui/clipboard';

interface TransferItemProps {
  transfer: BridgeTransfer;
}

export const TransferItem = ({ transfer }: TransferItemProps) => {
  const amountBN = BigNumber.from(transfer.amount);
  const feeBN = BigNumber.from(transfer.fee);

  const amountFrom = amountBN.add(feeBN);
  const amountTo = amountBN;

  const [isMobile] = useMediaQuery(['(max-width: 992px)'], { ssr: false });

  return (
    <Accordion.Root collapsible>
      <Accordion.Item
        value={transfer.hash}
        borderRadius="20px"
        overflow="hidden"
        bgColor={{ base: 'white', _dark: '#192B43' }}
        border="1px solid"
        borderColor={{ base: 'light', _dark: 'darkLine' }}
        style={{
          filter: 'drop-shadow(0px 10px 20px rgba(75, 105, 139, 0.03))',
        }}
      >
        <Accordion.ItemTrigger>
          <Flex align="center" justify="space-between" w="full" py={3} px={4} borderRadius="md">
            {!isMobile ? (
              <Accordion.ItemIndicator
                w="24px"
                h="24px"
                border="1px solid"
                borderColor={{ base: 'light1', _dark: 'darkLine' }}
                borderRadius="50px"
                display="flex"
                justifyContent="center"
                alignItems="center"
                fontSize="10px"
                color="main"
                mr="9px"
              >
                <FaAngleDown />
              </Accordion.ItemIndicator>
            ) : (
              <Flex mr="9px" transform="rotate(90deg)" color={{ base: 'grayTwo', _dark: '#8694A5' }} fontSize="14px">
                <BsRepeat />
              </Flex>
            )}
            {/* From Section */}
            <Grid
              gap={isMobile ? 1 : 2}
              templateColumns={isMobile ? '1fr' : 'auto 1fr auto'}
              w={!isMobile ? 'calc(100% - 40px)' : 'calc(100% - 40px)'}
            >
              <Flex align="center">
                <Image
                  src={`/img/${transfer.fromChain.identifier}.svg`}
                  alt={transfer.fromChain.name}
                  w="13px"
                  h="13px"
                  mr="2px"
                />
                <Text fontSize="sm" fontWeight="medium" mb={0} ml="2px">
                  {transfer.fromChain.name}:{' '}
                  <strong>
                    {utils.formatUnits(trimDecimals(amountFrom, transfer.token.decimals, 4), transfer.token.decimals)}
                  </strong>
                </Text>
                <Image
                  src={`/img/${transfer.token.identifier}.svg`}
                  alt={transfer.token.name}
                  w="13px"
                  h="13px"
                  ml="8px"
                />
                <Text fontSize="sm" mb={0} ml="4px">
                  {transfer.token.symbol}
                </Text>
              </Flex>

              {!isMobile && (
                <Flex justifyContent="center" alignItems="center">
                  <Icon as={LiaLongArrowAltRightSolid} boxSize={5} color={{ base: 'grayTwo', _dark: '#8694A5' }} />
                </Flex>
              )}

              {/* To Section */}
              <Flex align="center">
                <Image
                  src={`/img/${transfer.toChain.identifier}.svg`}
                  alt={transfer.toChain.name}
                  w="13px"
                  h="13px"
                  mr="4px"
                />
                <Text fontSize="sm" fontWeight="medium" mb={0}>
                  {transfer.toChain.name}:{' '}
                  <strong>
                    {utils.formatUnits(trimDecimals(amountTo, transfer.token.decimals, 4), transfer.token.decimals)}
                  </strong>
                </Text>
                <Image
                  src={`/img/${transfer.token.identifier}.svg`}
                  alt={transfer.token.name}
                  w="13px"
                  h="13px"
                  ml="8px"
                />
                <Text fontSize="sm" mb={0} ml="4px">
                  {transfer.token.symbol}
                </Text>
              </Flex>
            </Grid>
            {isMobile && (
              <Accordion.ItemIndicator
                w="24px"
                h="24px"
                border="1px solid"
                borderColor="light1"
                borderRadius="50px"
                display="flex"
                justifyContent="center"
                alignItems="center"
                fontSize="10px"
                color="main"
                ml="9px"
              >
                <FaAngleDown />
              </Accordion.ItemIndicator>
            )}
          </Flex>
        </Accordion.ItemTrigger>

        <Accordion.ItemContent borderTop="1px solid" borderColor={{ base: 'light', _dark: 'darkLine' }}>
          <Box bgColor={{ base: 'white', _dark: '#192B43' }} p={4} mt={1} fontSize=".875rem">
            <Flex direction="column" gap={1}>
              <Flex>
                <Text fontWeight="medium" mb={0}>
                  Sender:
                </Text>
                <Link
                  fontFamily="var(--font-noto-sans-mono)"
                  href={getAddressLink(transfer.fromChain, transfer.sender)}
                  ml={2}
                  color={{ base: 'dark', _dark: 'light' }}
                  target="_blank"
                  _hover={{ color: 'main' }}
                  outline="none"
                  wordBreak="break-all"
                >
                  {transfer.sender}
                </Link>
                <ClipboardRoot value={transfer.sender}>
                  <Tooltip content="Copy to clipboard" showArrow openDelay={50} closeDelay={100}>
                    <ClipboardIconButton p={0} w="14px" h="14px" icon={<FiCopy />} />
                  </Tooltip>
                </ClipboardRoot>
              </Flex>

              <Flex>
                <Text fontWeight="medium" mb={0}>
                  Receiver:
                </Text>
                <Link
                  fontFamily="var(--font-noto-sans-mono)"
                  href={getAddressLink(transfer.toChain, transfer.receiver)}
                  ml={2}
                  color={{ base: 'dark', _dark: 'light' }}
                  target="_blank"
                  _hover={{ color: 'main' }}
                  outline="none"
                  wordBreak="break-all"
                >
                  {transfer.receiver}
                </Link>
                <ClipboardRoot value={transfer.receiver}>
                  <Tooltip content="Copy to clipboard" showArrow openDelay={50} closeDelay={100}>
                    <ClipboardIconButton p={0} w="14px" h="14px" icon={<FiCopy />} />
                  </Tooltip>
                </ClipboardRoot>
              </Flex>

              <Flex align="center" fontSize=".8125rem">
                <Text fontWeight="medium" color={{ base: 'grayTwo', _dark: '#8694A5' }} mb={0}>
                  Status:
                </Text>
                <Text fontWeight="bold" color="green.500" ml={2} mb={0}>
                  Success
                </Text>
              </Flex>
            </Flex>
          </Box>
        </Accordion.ItemContent>
      </Accordion.Item>
    </Accordion.Root>
  );
};
export const SkeletonTransferItem = () => {
  return (
    <Box
      p="17px 20px"
      borderRadius="20px"
      bgColor={{ base: 'white', _dark: '#192B43' }}
      border="1px solid"
      borderColor={{ base: 'light', _dark: 'darkLine' }}
      filter="drop-shadow(0px 10px 20px rgba(75, 105, 139, 0.03))"
    >
      <Skeleton
        variant="shine"
        width="full"
        height="8px"
        mb="5px"
        css={{
          base: {
            '--start-color': '#e4edf8',
            '--end-color': '#e4edf87d',
          },
          _dark: {
            '--start-color': '#2A3B55',
            '--end-color': '#3B4E6B',
          },
        }}
      />
      <Skeleton
        variant="shine"
        width="full"
        height="8px"
        css={{
          base: {
            '--start-color': '#e4edf8',
            '--end-color': '#e4edf87d',
          },
          _dark: {
            '--start-color': '#2A3B55',
            '--end-color': '#3B4E6B',
          },
        }}
      />
    </Box>
  );
};
