import React, { useState, useEffect } from 'react';
import { getSupportedChains, getSupportedTokens } from '@src/config';
import { Chain, Token } from '@src/types';
import { Input, Box, HStack, Image, createListCollection, Grid } from '@chakra-ui/react';
import { SelectRoot, SelectTrigger, SelectValueText, SelectContent, SelectItem } from '@/components/ui/select';
import { InputGroup } from '@/components/ui/input-group';
import { LuSearch } from 'react-icons/lu';

interface SelectChainTokenProps {
  initialChain: Chain;
  initialToken: Token;
  onChangeChain: (chain: Chain) => void;
  onChangeToken: (token: Token) => void;
}

const SelectChainToken = ({ initialChain, initialToken, onChangeChain, onChangeToken }: SelectChainTokenProps) => {
  const chains = getSupportedChains();
  const tokens = getSupportedTokens();

  const [selectedChain, setSelectedChain] = useState<Chain>(initialChain);
  const [selectedToken, setSelectedToken] = useState<Token>(initialToken);

  const [chainFilter, setChainFilter] = useState('');
  const [tokenFilter, setTokenFilter] = useState('');

  useEffect(() => {
    setSelectedChain(initialChain);
    setSelectedToken(initialToken);
  }, [initialChain, initialToken]);

  const resetFilter = () => {
    setChainFilter('');
    setTokenFilter('');
  };

  const chainsList = createListCollection({
    items: chains,
    itemToString: (item) => item.name,
    itemToValue: (item) => item.identifier,
  });

  const tokensList = createListCollection({
    items: tokens,
    itemToString: (item) => `${item.name} (${item.symbol})`,
    itemToValue: (item) => item.identifier,
  });

  return (
    <Grid templateColumns="1fr 1fr" gap={2}>
      <Box>
        <SelectRoot
          size="sm"
          width="full"
          collection={chainsList}
          value={[selectedChain.identifier]}
          onValueChange={(valueDetails) => {
            const value = valueDetails.value[0];
            const newChain = chains.find((chain) => chain.identifier === value);
            if (newChain) {
              setSelectedChain(newChain);
              onChangeChain(newChain);
              resetFilter();
            }
          }}
          positioning={{ sameWidth: true }}
        >
          <SelectTrigger
            className="custom-select-trigger"
            border="1px solid"
            borderColor={{ base: 'light', _dark: 'darkLine' }}
            bgColor={{ base: 'white', _dark: '#33475F' }}
            borderRadius="full"
            height="36px"
            width="full"
            textAlign="left"
            fontWeight="500"
            fontSize="0.875rem"
            display="flex"
            alignItems="center"
            px="0"
          >
            <HStack w="100%">
              <Image alt={selectedChain.name} src={`/img/${selectedChain.identifier}.svg`} boxSize="16px" />
              <SelectValueText placeholder="Select Blockchain">
                {(items) => (items.length ? items[0].name : 'Select Blockchain')}
              </SelectValueText>
              {/*<Icon as={FaChevronDown} />*/}
            </HStack>
          </SelectTrigger>
          <SelectContent
            portalled={false}
            bgColor={{ base: 'white', _dark: '#33475F' }}
            border="1px solid"
            borderColor={{ base: 'light', _dark: 'darkLine' }}
            boxShadow="0px 5px 10px rgba(0, 0, 0, 0.08)"
            borderRadius="12px"
            p="16px"
          >
            <Box py={2}>
              <InputGroup flex="1" startElement={<LuSearch />}>
                <Input
                  size="sm"
                  placeholder="Search..."
                  bgColor={{ base: 'light', _dark: '#33475F' }}
                  borderColor={{ base: 'light', _dark: 'darkLine' }}
                  outline="none"
                  fontSize=".875rem"
                  borderRadius="full"
                  value={chainFilter}
                  onChange={(e) => setChainFilter(e.target.value)}
                />
              </InputGroup>
            </Box>
            {chains
              .filter(
                (chain) =>
                  chain.name.toLowerCase().includes(chainFilter.toLowerCase()) ||
                  chain.identifier.toLowerCase().includes(chainFilter.toLowerCase()),
              )
              .map((chain) => (
                <SelectItem
                  key={chain.identifier}
                  item={chain}
                  justifyContent="flex-start"
                  onSelect={() => setSelectedChain(chain)}
                  border="1px solid"
                  borderColor={{ base: 'light', _dark: 'darkLine' }}
                  mt="3px"
                  h="33px"
                  p="6px 10px"
                  borderRadius="9px"
                  cursor="pointer"
                  _hover={{
                    borderColor: 'lightBlue',
                    bg: 'transparent',
                  }}
                >
                  <HStack w="100%">
                    <Image alt={chain.name} src={`/img/${chain.identifier}.svg`} boxSize="16px" />
                    {chain.name}
                  </HStack>
                </SelectItem>
              ))}
          </SelectContent>
        </SelectRoot>
      </Box>

      <Box>
        <SelectRoot
          className="custom-select-trigger"
          collection={tokensList}
          value={[selectedToken.identifier]}
          onValueChange={(valueDetails) => {
            const value = valueDetails.value[0];
            const newToken = tokens.find((token) => token.identifier === value);
            if (newToken) {
              setSelectedToken(newToken);
              onChangeToken(newToken);
              resetFilter();
            }
          }}
          positioning={{ sameWidth: true }}
        >
          <SelectTrigger
            className="custom-select-trigger"
            border="1px solid"
            borderColor={{ base: 'light', _dark: 'darkLine' }}
            bgColor={{ base: 'white', _dark: '#33475F' }}
            borderRadius="full"
            height="36px"
            width="full"
            textAlign="left"
            fontWeight="500"
            fontSize="0.875rem"
            display="flex"
            alignItems="center"
            px="0"
          >
            <HStack w="100%">
              <Image alt={selectedToken.name} src={`/img/${selectedToken.identifier}.svg`} boxSize="16px" />
              <SelectValueText placeholder="Select Token">
                {(items) => (items.length ? `${items[0].name} (${items[0].symbol})` : 'Select Token')}
              </SelectValueText>
            </HStack>
          </SelectTrigger>
          <SelectContent
            portalled={false}
            bgColor={{ base: 'white', _dark: '#33475F' }}
            border="1px solid"
            borderColor={{ base: 'light', _dark: 'darkLine' }}
            boxShadow="0px 5px 10px rgba(0, 0, 0, 0.08)"
            borderRadius="12px"
            p="16px"
          >
            <Box py={2}>
              <InputGroup flex="1" startElement={<LuSearch />}>
                <Input
                  size="sm"
                  placeholder="Search..."
                  bgColor={{ base: 'light2', _dark: '#33475F' }}
                  borderColor={{ base: 'light', _dark: 'darkLine' }}
                  outline="none"
                  fontSize=".875rem"
                  borderRadius="full"
                  value={tokenFilter}
                  onChange={(e) => setTokenFilter(e.target.value)}
                />
              </InputGroup>
            </Box>
            {tokens
              .filter(
                (token) =>
                  token.name.toLowerCase().includes(tokenFilter.toLowerCase()) ||
                  token.symbol.toLowerCase().includes(tokenFilter.toLowerCase()),
              )
              .map((token) => (
                <SelectItem
                  key={token.identifier}
                  item={token}
                  justifyContent="flex-start"
                  onSelect={() => setSelectedToken(token)}
                  border="1px solid"
                  borderColor="light"
                  mt="3px"
                  h="33px"
                  p="6px 10px"
                  borderRadius="9px"
                  cursor="pointer"
                  _hover={{
                    borderColor: 'lightBlue',
                    bg: 'transparent',
                  }}
                >
                  <HStack>
                    <Image alt={token.name} src={`/img/${token.identifier}.svg`} boxSize="16px" />
                    {token.symbol} - {token.name}
                  </HStack>
                </SelectItem>
              ))}
          </SelectContent>
        </SelectRoot>
      </Box>
    </Grid>
  );
};

export default SelectChainToken;
