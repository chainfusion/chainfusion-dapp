'use client';

import { useEffect } from 'react';
import { TransferItem, SkeletonTransferItem } from '@components/Bridge/TransferItem';
import { useBridgeStore } from '@store/bridge';
import { BridgeTransfer } from '@src/types';
import { useAPI } from '@src/hooks/useAPI';
import { AnimatePresence, motion } from 'framer-motion';
import { getNativeChain } from '@src/config';
import { Grid, Box, Heading, Text, Flex, Stack, Link } from '@chakra-ui/react';
import { Button } from '@src/components/ui/button';
import { useAccount } from 'wagmi';

const TransferHistory = () => {
  const { isConnected } = useAccount();
  const nativeChain = getNativeChain();
  const { loadHistory } = useAPI();

  const { history, historyLoading, historyItemsToShow, onlyMyHistory, setHistoryItemsToShow, setOnlyMyHistory } =
    useBridgeStore();

  useEffect(() => {
    loadHistory().then();
  }, [loadHistory]);

  const transactionItems = history
    .filter((transfer: BridgeTransfer, index: number) => index < historyItemsToShow)
    .map((transfer: BridgeTransfer) => <TransferItem key={transfer.hash} transfer={transfer} />);
  return (
    <Box w="100%" maxW="540px" mx="auto" mb="8" mt="54px">
      <Grid templateColumns="1fr auto" justifyContent="flex-start" alignItems="center" mb="25px">
        <Heading as="h2" fontSize="28px" fontWeight={600} mb={0}>
          Recent Transfers
        </Heading>

        {isConnected && (
          <Flex borderRadius="7px" overflow="hidden" gap={0}>
            <Button
              onClick={() => setOnlyMyHistory(false)}
              bg={{
                base: !onlyMyHistory ? 'main' : 'light1',
                _dark: !onlyMyHistory ? 'blue.500' : '#192B43',
              }}
              color={{
                base: !onlyMyHistory ? 'white' : 'grayTwo',
                _dark: !onlyMyHistory ? 'white' : 'gray.400',
              }}
              _hover={{
                bg: {
                  base: !onlyMyHistory ? 'blue.600' : 'gray.200',
                  _dark: !onlyMyHistory ? 'blue.700' : 'darkLine',
                },
              }}
              fontSize=".75rem"
              borderRadius={0}
              border={0}
              h="26px"
              px="10px"
              w="auto"
            >
              All
            </Button>

            <Button
              onClick={() => setOnlyMyHistory(true)}
              bg={{
                base: onlyMyHistory ? 'main' : 'light1',
                _dark: onlyMyHistory ? 'blue.500' : '#192B43',
              }}
              color={{
                base: onlyMyHistory ? 'white' : 'grayTwo',
                _dark: onlyMyHistory ? 'white' : 'gray.400',
              }}
              _hover={{
                bg: {
                  base: onlyMyHistory ? 'blue.600' : 'gray.200',
                  _dark: onlyMyHistory ? 'blue.700' : 'darkLine',
                },
              }}
              fontSize=".75rem"
              borderRadius={0}
              border={0}
              h="26px"
              px="10px"
              w="auto"
            >
              Only My
            </Button>
          </Flex>
        )}
      </Grid>

      <AnimatePresence>
        <motion.div
          key={`only-my-${onlyMyHistory}`}
          initial={{ opacity: 0, y: 30 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ duration: 0.15 }}
        >
          {historyLoading ? (
            <Stack gap=".5rem">
              <SkeletonTransferItem />
              <SkeletonTransferItem />
              <SkeletonTransferItem />
              <SkeletonTransferItem />
              <SkeletonTransferItem />
            </Stack>
          ) : transactionItems.length > 0 ? (
            <Stack gap="8px">{transactionItems}</Stack>
          ) : (
            <Text
              bgColor={{ base: 'white', _dark: '#192B43' }}
              border="1px solid"
              borderColor={{ base: 'light', _dark: 'darkLine' }}
              borderRadius="md"
              p={4}
              textAlign="center"
              color={{ base: 'dark', _dark: 'light' }}
              fontSize="14px"
              my="4"
              boxShadow="0 17px 40px #232b4014"
            >
              Nothing to show for last{' '}
              <Link href={nativeChain.explorer + '/blocks'} color="blue.400">
                100k blocks
              </Link>
              , new transactions will appear here.
            </Text>
          )}

          {history.length > historyItemsToShow && !historyLoading && (
            <Flex justify="center" mt="4">
              <Button variant="outline" size="sm" onClick={() => setHistoryItemsToShow(historyItemsToShow + 5)}>
                Show More
              </Button>
            </Flex>
          )}
        </motion.div>
      </AnimatePresence>
    </Box>
  );
};

export default TransferHistory;
