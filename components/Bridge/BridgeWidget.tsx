'use client';

import { ChangeEvent, useEffect, useState } from 'react';
import { BigNumber, BytesLike, ethers, Event, utils } from 'ethers';
import { ERC20Bridge, MockToken__factory } from '@chainfusion/erc-20-bridge-contracts';
import FeeEstimate from '@components/Bridge/FeeEstimate';
import OptionsModal from '@components/Modals/OptionsModal';
import TransferModal from '@components/Modals/TransferModal';
import { getChain, getToken, getSupportedChains, getSupportedTokens, getChainById, getNativeChain } from '@src/config';
import { useLocalStorage } from '@src/hooks/useLocalStorage';
import { useChainContext } from '@src/context/ChainContext';
import { Chain, Token } from '@src/types';
import { EventRegistry, RelayBridge } from '@chainfusion/chainfusion-contracts';
import {
  decodeBridgeTransfer,
  decodeSendEventData,
  getTransactionLink,
  oneEther,
  parseUnits,
  trimDecimals,
} from '@src/utils';
import { useAPI } from '@src/hooks/useAPI';
import { useBridgeStore } from '@store/bridge';

import { Box, Input, Image, IconButton, Flex, Heading } from '@chakra-ui/react';
import { FaCog, FaCheck } from 'react-icons/fa';
import { VscSend } from 'react-icons/vsc';
import { LuArrowUpDown } from 'react-icons/lu';
import { PiWallet, PiShuffle } from 'react-icons/pi';
import { motion } from 'framer-motion';
import { Toaster, toaster } from '@/components/ui/toaster';

import { Button } from '@src/components/ui/button';
import { useAccount, useNetwork, useSwitchNetwork } from 'wagmi';
import SelectChainToken from '@components/Bridge/SelectChainToken';

interface FeeInfo {
  validatorsFee: BigNumber;
  liquidityFee: BigNumber;
}

enum InputField {
  FROM,
  TO,
}

interface AmountInput {
  field: InputField;
  amount: BigNumber;
}

const BridgeWidget = () => {
  const { switchNetwork } = useSwitchNetwork();
  const [showOptionsModal, setShowOptionsModal] = useState(false);
  const [showTransferModal, setShowTransferModal] = useState(false);

  const [updatePending, setUpdatePending] = useState(false);
  const [approvalPending, setApprovalPending] = useState(false);
  const [transferPending, setTransferPending] = useState(false);
  const [needsApproval, setNeedsApproval] = useState(true);
  const [insufficientBalance, setInsufficientBalance] = useState(false);

  const isLoading = approvalPending || transferPending;

  const [amountInput, setAmountInput] = useState<AmountInput>({ field: InputField.FROM, amount: BigNumber.from(0) });

  const [validatorsFee, setValidatorsFee] = useState<BigNumber | undefined>(undefined);
  const [tokenFeePercentage, setTokenFeePercentage] = useState<BigNumber | undefined>(undefined);
  const [estimatedFee, setEstimatedFee] = useState<FeeInfo>({
    validatorsFee: BigNumber.from(0),
    liquidityFee: BigNumber.from(0),
  });

  const [transferStage, setTransferStage] = useState<number>(1);
  const [stageLinks, setStageLinks] = useState(new Map<number, string>());

  const [swap, setSwap] = useState(false);

  const { receiver } = useBridgeStore();

  const [chainFromLocal, setChainFrom] = useLocalStorage<string | undefined>('chain-from');
  const [tokenFromLocal, setTokenFrom] = useLocalStorage<string>('token-from');
  const [chainToLocal, setChainTo] = useLocalStorage<string | undefined>('chain-to');
  const [tokenToLocal, setTokenTo] = useLocalStorage<string>('token-to');

  const chains = getSupportedChains();
  const tokens = getSupportedTokens();

  let chainFrom = chainFromLocal ? getChain(chainFromLocal) : chains[0];
  let chainTo = chainToLocal ? getChain(chainToLocal) : chains[1];

  let tokenFrom = tokenFromLocal ? getToken(tokenFromLocal) : tokens[0];
  let tokenTo = tokenToLocal ? getToken(tokenToLocal) : tokens[0];

  if (!chainFrom) chainFrom = chains[0];
  if (!chainTo) chainTo = chains[1];
  if (!tokenFrom) tokenFrom = tokens[0];
  if (!tokenTo) tokenTo = tokens[0];

  const tokenFromAddress = tokenFrom.chains[chainFrom.identifier];
  const tokenToAddress = tokenTo.chains[chainTo.identifier];

  const [balance, setBalance] = useState<BigNumber>(BigNumber.from(0));
  const [allowance, setAllowance] = useState<BigNumber>(BigNumber.from(0));

  const [fromString, setFromString] = useState<string>('');
  const from = parseUnits(fromString, tokenFrom.decimals);

  const [toString, setToString] = useState<string>('');
  const to = parseUnits(toString, tokenTo.decimals);

  const { isConnected: isActive } = useAccount();
  const { chain } = useNetwork();

  const chainId = chain?.id;

  const { signerAccount, networkContainer, nativeContainer, showConnectWalletDialog } = useChainContext();

  const { loadHistory } = useAPI();

  const networkFrom = networkContainer.get(chainFrom.identifier);
  const networkTo = networkContainer.get(chainTo.identifier);

  const swapFromTo = () => {
    setChainFrom(chainTo.identifier);
    setTokenFrom(tokenTo.identifier);

    setChainTo(chainFrom.identifier);
    setTokenTo(tokenFrom.identifier);

    setSwap(!swap);
  };

  useEffect(() => {
    if (
      amountInput.amount.eq(0) ||
      networkFrom === undefined ||
      networkFrom.contracts === undefined ||
      tokenFromAddress === undefined ||
      validatorsFee === undefined ||
      tokenFeePercentage === undefined
    ) {
      setInsufficientBalance(false);
      setNeedsApproval(false);
      setEstimatedFee({
        validatorsFee: BigNumber.from(0),
        liquidityFee: BigNumber.from(0),
      });

      if (amountInput.field === InputField.FROM) setToString('');
      if (amountInput.field === InputField.TO) setFromString('');

      return;
    }

    let amount = BigNumber.from(0);
    let estimatedFee = BigNumber.from(0);

    if (amountInput.field === InputField.FROM) {
      amount = amountInput.amount;
      estimatedFee = calculateFee(amount, tokenFeePercentage, validatorsFee);
      setToString(utils.formatUnits(amount.sub(estimatedFee), tokenFrom.decimals));
    }

    if (amountInput.field === InputField.TO) {
      amount = amountInput.amount.add(validatorsFee).mul(oneEther).div(oneEther.sub(tokenFeePercentage));
      estimatedFee = calculateFee(amount, tokenFeePercentage, validatorsFee);
      setFromString(utils.formatEther(amount));
    }

    setInsufficientBalance(balance.lt(amount));
    setNeedsApproval(allowance.lt(amount));
    setEstimatedFee({
      validatorsFee: validatorsFee,
      liquidityFee: estimatedFee.sub(validatorsFee),
    });
  }, [
    amountInput,
    tokenFrom,
    tokenFromAddress,
    networkFrom,
    balance,
    allowance,
    approvalPending,
    validatorsFee,
    tokenFeePercentage,
  ]);

  useEffect(() => {
    let pending = true;

    setUpdatePending(true);

    const update = async () => {
      if (networkFrom?.contracts === undefined || tokenFromAddress === undefined) {
        if (pending) {
          setBalance(BigNumber.from(0));
          setAllowance(BigNumber.from(0));
          setUpdatePending(false);
        }
        return;
      }

      const mockTokenFactory = new MockToken__factory(networkFrom.provider.getSigner(signerAccount));
      const mockToken = mockTokenFactory.attach(tokenFromAddress);
      const balancePromise = mockToken.balanceOf(networkFrom.account);
      const allowancePromise = mockToken.allowance(networkFrom.account, networkFrom.contracts.erc20Bridge.address);

      const validatorsFeePromise = networkFrom.contracts.feeManager.validatorRefundFee();
      const tokenFeePercentagePromise = networkFrom.contracts.feeManager.tokenFeePercentage(tokenFromAddress);

      const balance = await balancePromise;
      const allowance = await allowancePromise;
      const validatorsFee = await validatorsFeePromise;
      const tokenFeePercentage = await tokenFeePercentagePromise;

      if (pending) {
        setBalance(balance);
        setAllowance(allowance);
        setValidatorsFee(validatorsFee);
        setTokenFeePercentage(tokenFeePercentage);
        setUpdatePending(false);
      }
    };

    update().then();

    return () => {
      pending = false;
    };
  }, [signerAccount, networkFrom, tokenFromAddress, approvalPending, transferPending]);

  const approve = async () => {
    if (
      networkFrom === undefined ||
      !networkFrom.connected ||
      networkFrom.contracts === undefined ||
      tokenFromAddress === undefined
    ) {
      return;
    }

    setApprovalPending(true);

    try {
      const { erc20Bridge } = networkFrom.contracts;
      const mockTokenFactory = new MockToken__factory(networkFrom.provider.getSigner(signerAccount));
      const mockToken = mockTokenFactory.attach(tokenFromAddress);
      const amount = from;

      await (await mockToken.approve(erc20Bridge.address, amount)).wait();

      toaster.create({
        title: 'Success',
        // eslint-disable-next-line max-len
        description: `Successfully approved ${ethers.utils.formatUnits(amount, tokenFrom.decimals)} ${tokenFrom.symbol} on ${chainFrom.name}`,
        type: 'success',
        duration: 3000,
        placement: 'top-end',
      });
    } catch (/* eslint-disable @typescript-eslint/no-explicit-any */ e: any) {
      if (typeof e.code === 'string') {
        const code = e.code as string;
        switch (code) {
          case 'ACTION_REJECTED':
            toaster.create({
              title: 'Info',
              description: 'Token approval was canceled',
              type: 'info',
            });
            setShowTransferModal(false);
            break;
        }
      }

      console.error(e);
    }

    setApprovalPending(false);
  };

  const transfer = async () => {
    if (
      networkFrom === undefined ||
      !networkFrom.connected ||
      networkFrom.contracts === undefined ||
      networkTo?.contracts === undefined ||
      tokenFromAddress === undefined ||
      tokenToAddress === undefined ||
      nativeContainer === undefined
    ) {
      return;
    }

    setStageLinks(new Map());
    setTransferStage(1);
    setTransferPending(true);
    setShowTransferModal(true);

    const stageLinks = new Map<number, string>();

    try {
      const { erc20Bridge } = networkFrom.contracts;

      const amount = from;

      let depositReceiver = networkFrom.account;
      if (receiver !== undefined) {
        depositReceiver = receiver;
      }

      const onEventRegisteredPromise = onEventRegistered(
        nativeContainer.eventRegistry,
        networkFrom.contracts.relayBridge,
        {
          appContract: erc20Bridge.address,
          sender: networkFrom.account,
          receiver: depositReceiver,
          sourceChain: BigNumber.from(chainFrom.chainId),
          destinationChain: BigNumber.from(chainTo.chainId),
        },
      );

      const onTransferCompletePromise = onTransferComplete(networkTo.contracts.erc20Bridge, {
        sender: networkFrom.account,
        receiver: depositReceiver,
        token: tokenToAddress,
        amount: amount,
      });

      const depositTx = await (
        await erc20Bridge.deposit(tokenFromAddress, chainTo.chainId, depositReceiver, amount)
      ).wait(1);

      setStageLinks(new Map(stageLinks.set(1, getTransactionLink(networkFrom.chain, depositTx.transactionHash))));
      setTransferStage(2);

      const registeredEvent = await onEventRegisteredPromise;

      setStageLinks(new Map(stageLinks.set(2, getTransactionLink(getNativeChain(), registeredEvent.transactionHash))));
      setTransferStage(3);

      const transferCompleteEvent = await onTransferCompletePromise;

      setStageLinks(
        new Map(stageLinks.set(3, getTransactionLink(networkTo.chain, transferCompleteEvent.transactionHash))),
      );
      setTransferStage(4);

      loadHistory().then();

      toaster.create({
        title: 'Success',
        // eslint-disable-next-line max-len
        description: `Successfully transferred ${utils.formatUnits(amount, tokenFrom.decimals)} ${tokenFrom.symbol} to ${chainTo.name}`,
        type: 'success',
      });
    } catch (e: any) {
      if (typeof e.code === 'string') {
        const code = e.code as string;
        switch (code) {
          case 'ACTION_REJECTED':
            toaster.create({
              title: 'Info',
              description: 'Transfer was canceled',
              type: 'info',
            });
            setShowTransferModal(false);
            break;
        }
      }
    }

    setFromString('');
    setToString('');
    setAmountInput({ field: InputField.FROM, amount: BigNumber.from(0) });
    setTransferPending(false);
  };

  const transferButton = () => {
    if (from.eq(0) && to.eq(0)) {
      return <Button disabled>Enter Amount</Button>;
    }

    if (updatePending) {
      return (
        <Button loading loadingText="Updating..." disabled>
          Updating...
        </Button>
      );
    }

    if (!isActive) {
      return (
        <Button onClick={() => showConnectWalletDialog()}>
          <PiWallet />
          Connect Wallet
        </Button>
      );
    }

    if (chainId !== chainFrom.chainId) {
      return (
        <Button onClick={() => switchNetwork?.(chainFrom.chainId)}>
          <Flex as="span" alignItems="center" gap="5px">
            <PiShuffle />
            Switch Network to <Image boxSize="18px" src={`/img/${chainFrom.identifier}.svg`} alt={chainFrom.name} />
            {chainFrom.name}
          </Flex>
        </Button>
      );
    }

    if (networkFrom?.contracts === undefined || tokenFromAddress === undefined) {
      return <Button disabled>Not Supported</Button>;
    }

    if (insufficientBalance) {
      return <Button disabled>Insufficient Balance</Button>;
    }

    if (approvalPending) {
      return (
        <Button loading loadingText="Approving..." disabled>
          Approving
        </Button>
      );
    }

    if (transferPending) {
      return (
        <Button loading loadingText="Transferring..." disabled>
          Transferring
        </Button>
      );
    }

    if (needsApproval) {
      return (
        <Button onClick={() => approve()}>
          <FaCheck style={{ width: '14px' }} />
          Approve
        </Button>
      );
    }

    return (
      <Button onClick={() => transfer()}>
        <VscSend style={{ width: '18px' }} />
        Transfer
      </Button>
    );
  };

  return (
    <Box
      position="relative"
      mt="120px"
      p="32px 40px"
      bgColor={{ base: 'white', _dark: '#192B43' }}
      border="1px solid"
      borderColor={{ base: 'light', _dark: 'darkLine' }}
      borderRadius="24px"
      boxShadow="0px 10px 24px rgba(75, 105, 139, 0.07)"
    >
      <IconButton
        aria-label="Settings"
        position="absolute"
        top="4"
        right="4"
        fontSize="lg"
        color="main"
        bg="transparent"
        onClick={() => setShowOptionsModal(true)}
      >
        <FaCog />
      </IconButton>

      <Flex justifyContent="center">
        <Heading as="h2" fontSize="28px" fontWeight={400} mb={8}>
          Bridge Tokens
        </Heading>
      </Flex>

      {/* FROM SECTION */}
      <SelectChainToken
        key={`from:${chainFrom.identifier}:${tokenFrom.identifier}`}
        initialChain={chainFrom}
        initialToken={tokenFrom}
        onChangeChain={(chain: Chain) => {
          if (chainTo.chainId == chain.chainId) {
            setChainTo(chainFrom.identifier);
          }

          setChainFrom(chain.identifier);
        }}
        onChangeToken={(token: Token) => {
          setTokenFrom(token.identifier);

          setTokenTo(token.identifier);
        }}
      />
      <Box pos="relative" mt={2}>
        <Input
          type="text"
          autoComplete="off"
          placeholder="0"
          fontSize="16px"
          fontWeight="500"
          borderRadius="full"
          bg={{ base: 'rgba(231,238,245,.52)', _dark: '#33475F' }}
          color={{ base: 'dark', _dark: 'light' }}
          px="16px"
          h="52px"
          border="1px solid transparent"
          outline="none"
          disabled={isLoading}
          value={fromString}
          onChange={(e: ChangeEvent<HTMLInputElement>) => {
            const input = e.target.value;
            if (/^[0-9\b.]*$/.test(input)) {
              setFromString(input);
              const amount = parseUnits(input, tokenFrom.decimals);
              setAmountInput({ field: InputField.FROM, amount: amount });
            }
          }}
        />
      </Box>

      <Flex justifyContent="center" my="-4" pos="relative" zIndex={1}>
        <motion.div animate={swap ? { rotate: 360 } : { rotate: 0 }} transition={{ duration: 0.3, ease: 'easeInOut' }}>
          <IconButton
            aria-label="Swap"
            size="lg"
            bgColor={{ base: 'white', _dark: '#7483A6' }}
            color={{ base: 'main', _dark: 'white' }}
            border="1px solid"
            borderColor={{ base: 'light', _dark: '#7483A6' }}
            borderRadius="full"
            boxShadow="0 0 21px hsla(204,2%,51%,.08)"
            onClick={swapFromTo}
            disabled={isLoading}
          >
            <LuArrowUpDown />
          </IconButton>
        </motion.div>
      </Flex>

      {/* TO SECTION */}
      <Box mb="2" pos="relative">
        <Input
          type="text"
          autoComplete="off"
          placeholder="0"
          fontSize="16px"
          fontWeight="500"
          borderRadius="full"
          bg={{ base: 'rgba(231,238,245,.52)', _dark: '#33475F' }}
          color={{ base: 'dark', _dark: 'light' }}
          px="16px"
          h="52px"
          border="1px solid transparent"
          outline="none"
          disabled={isLoading}
          value={toString}
          onChange={(e: ChangeEvent<HTMLInputElement>) => {
            const input = e.target.value;
            if (/^[0-9\b.]*$/.test(input)) {
              setToString(input);
              const amount = parseUnits(input, tokenTo.decimals);
              setAmountInput({ field: InputField.TO, amount: amount });
            }
          }}
        />
      </Box>
      <Box mb="32px">
        <SelectChainToken
          key={`to:${chainTo.identifier}:${tokenTo.identifier}`}
          initialChain={chainTo}
          initialToken={tokenTo}
          onChangeToken={(token: Token) => {
            setTokenTo(token.identifier);

            setTokenFrom(token.identifier);
          }}
          onChangeChain={(chain: Chain) => {
            if (chainFrom.chainId == chain.chainId) {
              setChainFrom(chainTo.identifier);
            }

            setChainTo(chain.identifier);
          }}
        />
      </Box>
      {transferButton()}

      {balance.gt(0) && (
        <Flex justifyContent="center" fontSize="14px" fontStyle="italic" color="grayTwo" mt="12px">
          Maximum amount for this swap is
          <Box
            as="button"
            color={{ base: 'dark', _dark: 'white' }}
            ml="1"
            cursor="pointer"
            onClick={() => {
              setFromString(ethers.utils.formatUnits(balance, tokenFrom.decimals));
              setAmountInput({
                field: InputField.FROM,
                amount: balance,
              });
            }}
          >
            {ethers.utils.formatUnits(trimDecimals(balance, tokenFrom.decimals, 4), tokenFrom.decimals)}{' '}
            {tokenFrom.symbol}
          </Box>
        </Flex>
      )}

      <OptionsModal show={showOptionsModal} close={() => setShowOptionsModal(false)} />
      <TransferModal
        show={showTransferModal}
        stage={transferStage}
        stageLinks={stageLinks}
        close={() => setShowTransferModal(false)}
      />

      {estimatedFee.validatorsFee.add(estimatedFee.liquidityFee).gt(0) && (
        <FeeEstimate
          token={tokenFrom}
          validatorsFee={estimatedFee.validatorsFee}
          liquidityFee={estimatedFee.liquidityFee}
          isEstimating={updatePending}
        />
      )}
      <Toaster />
    </Box>
  );
};

export default BridgeWidget;

interface EventRegisteredFilter {
  appContract: string;
  sender: string;
  receiver: string;
  sourceChain: BigNumber;
  destinationChain: BigNumber;
}

async function onEventRegistered(
  eventRegistry: EventRegistry,
  relayBridge: RelayBridge,
  filter: EventRegisteredFilter,
): Promise<Event> {
  return new Promise<Event>((resolve) => {
    eventRegistry.once(
      'EventRegistered',
      async (
        hash: BytesLike,
        appContract: string,
        sourceChain: BigNumber,
        destinationChain: BigNumber,
        data: BytesLike,
        validatorFee: BigNumber,
        eventType: BigNumber,
        event: Event,
      ) => {
        if (
          appContract !== filter.appContract ||
          !sourceChain.eq(filter.sourceChain) ||
          !destinationChain.eq(filter.destinationChain)
        ) {
          return;
        }

        const fromChain = getChainById(sourceChain.toNumber());
        const toChain = getChainById(destinationChain.toNumber());

        if (fromChain === undefined || toChain === undefined) {
          return;
        }

        const sentData = decodeSendEventData(data);
        if (sentData === undefined) {
          return;
        }

        const item = decodeBridgeTransfer(hash.toString(), fromChain, toChain, sentData);
        if (item === undefined || item.sender !== filter.sender || item.receiver !== filter.receiver) {
          return;
        }

        resolve(event);

        return false;
      },
    );
  });
}

interface TransferCompleteFilter {
  sender: string;
  receiver: string;
  token: string;
  amount: BigNumber;
}

async function onTransferComplete(erc20Bridge: ERC20Bridge, filter: TransferCompleteFilter): Promise<Event> {
  return new Promise<Event>((resolve) => {
    erc20Bridge.on(
      'Transferred',
      (
        nonce: BigNumber,
        sender: string,
        token: string,
        sourceChainId: BigNumber,
        receiver: string,
        amount: BigNumber,
        fee: BigNumber,
        event: Event,
      ) => {
        if (sender !== filter.sender || receiver !== filter.receiver || token !== filter.token) {
          return;
        }

        resolve(event);

        return false;
      },
    );
  });
}

const calculateFee = (amount: BigNumber, feePercentage: BigNumber, fixedFee: BigNumber) => {
  return fixedFee.add(feePercentage.mul(amount).div(oneEther));
};
