import { Token } from '@src/types';
import { BigNumber, utils } from 'ethers';
import { Flex, Text, Image, Accordion } from '@chakra-ui/react';
import { FaAngleDown } from 'react-icons/fa6';

interface FeeEstimateProps {
  token: Token;
  validatorsFee: BigNumber;
  liquidityFee: BigNumber;
  isEstimating: boolean;
}

const FeeEstimate = ({ token, validatorsFee, liquidityFee, isEstimating }: FeeEstimateProps) => {
  const totalFeeString = utils.formatUnits(validatorsFee.add(liquidityFee), token.decimals);
  const validatorsFeeString = utils.formatUnits(validatorsFee, token.decimals);
  const liquidityFeeString = utils.formatUnits(liquidityFee, token.decimals);

  return (
    <Accordion.Root collapsible>
      <Accordion.Item
        border="1px solid"
        borderColor={{ base: 'light', _dark: 'darkLine' }}
        borderRadius={{ base: 'full', _open: '12px' }}
        overflow="hidden"
        bgColor={{ base: 'white', _dark: '#33475F' }}
        value="fees"
        mt="32px"
      >
        <Accordion.ItemTrigger>
          <Flex
            justify="space-between"
            alignItems="center"
            py={0.5}
            px={4}
            bg={{ base: 'white', _dark: '#33475F' }}
            color={{ base: 'dark', _dark: 'light' }}
            w="100%"
          >
            <Text fontWeight="500" fontSize="0.875rem" display="flex" alignItems="center" mb={0} gap="4px">
              Fees:{' '}
              <Text as="span" color="main" fontWeight={700}>
                {isEstimating ? '...' : totalFeeString}
              </Text>
              <Flex as="span" alignItems="center" display="inline-flex">
                <Image src={`/img/${token.identifier}.svg`} alt={token.name} w="16px" h="16px" ml="2px" />
                <Text as="span" ml="4px" mb={0}>
                  {token.symbol}
                </Text>
              </Flex>
            </Text>
            <Accordion.ItemIndicator
              w="14px"
              h="24px"
              borderRadius="50px"
              display="flex"
              justifyContent="center"
              alignItems="center"
              fontSize="10px"
              color={{ base: 'main', _dark: 'light3' }}
            >
              <FaAngleDown />
            </Accordion.ItemIndicator>
          </Flex>
        </Accordion.ItemTrigger>

        <Accordion.ItemContent borderTop="1px solid" borderColor={{ base: 'light', _dark: 'darkLine' }} px={6} py={4}>
          <Flex align="center" gap="4px" mb={2}>
            <Text fontSize=".8125rem" fontWeight="500" color={{ base: 'grayTwo', _dark: '#8694A5' }} mb={0}>
              Validators Refund:
            </Text>
            <Flex align="center" gap="4px" fontSize=".8125rem">
              <Text color="main" fontWeight={700} mb={0}>
                {isEstimating ? '...' : validatorsFeeString}
              </Text>
              <Image src={`/img/${token.identifier}.svg`} alt={token.name} w="14px" h="14px" ml="2px" />
              <Text mb={0}>{token.symbol}</Text>
            </Flex>
          </Flex>

          <Flex align="center" gap="4px">
            <Text fontSize=".8125rem" fontWeight="500" color={{ base: 'grayTwo', _dark: '#8694A5' }} mb={0}>
              Liquidity Fee:
            </Text>
            <Flex align="center" gap="4px" fontSize=".8125rem">
              <Text color="main" fontWeight={700} mb={0}>
                {isEstimating ? '...' : liquidityFeeString}
              </Text>
              <Image src={`/img/${token.identifier}.svg`} alt={token.name} w="14px" h="14px" ml="2px" />
              <Text mb={0}>{token.symbol}</Text>
            </Flex>
          </Flex>
        </Accordion.ItemContent>
      </Accordion.Item>
    </Accordion.Root>
  );
};

export default FeeEstimate;
