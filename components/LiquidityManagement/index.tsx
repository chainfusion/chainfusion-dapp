'use client';

import { TokenLiquidityItem } from '@components/Liquidity/TokenLiquidityItem';
import InputTokenModal from '@components/Modals/InputTokenModal';
import { getChain, getSupportedTokens } from '@src/config';
import { Token } from '@src/types';
import { BigNumber } from 'ethers';
import { useState } from 'react';
import { Grid, Box, Stack, Heading, Image, Container, Breadcrumb } from '@chakra-ui/react';
import Link from 'next/link';
import { FaAngleRight } from 'react-icons/fa6';

const LiquidityManagement = ({ identifier }: { identifier: string }) => {
  const [showAddLiquidityModal, setShowAddLiquidityModal] = useState(false);
  const [showRemoveLiquidityModal, setShowRemoveLiquidityModal] = useState(false);
  const [selectedToken, setSelectedToken] = useState<Token>(getSupportedTokens()[0]);
  const [selectedAmount, setSelectedAmount] = useState<BigNumber>(BigNumber.from(0));

  console.log(identifier, '✅ identifier received in LiquidityManagement');

  const chain = getChain(identifier);

  if (!chain) {
    return (
      <Container maxW="container.md" py={10}>
        <Heading size="lg">Error: Chain not found</Heading>
        <Box color="gray.500">The specified chain {identifier} does not exist.</Box>
      </Container>
    );
  }

  const tokens = getSupportedTokens().filter((token) => token.chains[chain.identifier] !== undefined);

  return (
    <>
      <Container py={10}>
        <Breadcrumb.Root>
          <Breadcrumb.List mb="2rem">
            <Breadcrumb.Item display="flex" alignItems="center" fontWeight="500" fontSize="0.8125rem">
              <Breadcrumb.Link
                color={{ base: 'grayTwo', _dark: '#8694A5' }}
                outline="none"
                _hover={{ color: 'main' }}
                as={Link}
                href="/liquidity"
              >
                Liquidity
              </Breadcrumb.Link>
            </Breadcrumb.Item>

            <Breadcrumb.Separator fontSize="0.6875rem" px="0.1rem" color="#c8cfd8">
              <FaAngleRight />
            </Breadcrumb.Separator>

            <Breadcrumb.Item
              px="15px"
              borderRadius="50px"
              color="main"
              height="30px"
              display="flex"
              alignItems="center"
              fontSize="0.8125rem"
              bg={{ base: 'light3', _dark: '#192B43' }}
              border="1px solid"
              borderColor={{ base: 'light1', _dark: '#192B43' }}
            >
              <Breadcrumb.Link outline="none" color="main">
                {chain.name} Pool
              </Breadcrumb.Link>
            </Breadcrumb.Item>
          </Breadcrumb.List>
        </Breadcrumb.Root>

        <Stack direction="row" alignItems="center" mt={5} mb="25px" gap={3}>
          <Image boxSize="28px" src={`/img/${chain.identifier}.svg`} alt={`${chain.name} Logo`} />
          <Heading fontSize="1.5rem" fontWeight={600} mb={0}>
            {chain.name} Pool
          </Heading>
        </Stack>

        <Grid templateColumns={{ base: '1fr', md: '1fr 1fr' }} gap={{ base: '15px', lg: '30px' }}>
          {tokens.map((token) => (
            <TokenLiquidityItem
              key={token.identifier}
              chain={chain}
              token={token}
              onAddLiquidity={(token, amount) => {
                setSelectedToken(token);
                setSelectedAmount(amount);
                setShowAddLiquidityModal(true);
              }}
              onRemoveLiquidity={(token, amount) => {
                setSelectedToken(token);
                setSelectedAmount(amount);
                setShowRemoveLiquidityModal(true);
              }}
            />
          ))}
        </Grid>
      </Container>

      <InputTokenModal
        show={showAddLiquidityModal}
        token={selectedToken}
        maxValue={selectedAmount}
        maxValueText="Available to add"
        title="Add Liquidity"
        buttonText="Add"
        buttonIcon="fa-plus"
        submit={(amount) => console.log(`Liquidity add amount: ${amount}`)}
        close={() => setShowAddLiquidityModal(false)}
      />

      <InputTokenModal
        show={showRemoveLiquidityModal}
        token={selectedToken}
        maxValue={selectedAmount}
        maxValueText="Available to remove"
        title="Remove Liquidity"
        buttonText="Remove"
        buttonIcon="fa-minus"
        submit={(amount) => console.log(`Liquidity remove amount: ${amount}`)}
        close={() => setShowRemoveLiquidityModal(false)}
      />
    </>
  );
};

export default LiquidityManagement;
