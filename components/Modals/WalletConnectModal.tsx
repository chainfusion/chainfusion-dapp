import React, { memo } from 'react';
import NextImage from 'next/image';
import { Flex, Button, Icon } from '@chakra-ui/react';
import {
  DialogRoot,
  DialogContent,
  DialogHeader,
  DialogBody,
  DialogTitle,
  DialogCloseTrigger,
} from '@src/components/ui/dialog';

import { Connector, useDisconnect, useAccount, useSwitchNetwork } from 'wagmi';
import { walletConfig } from '@/lib/web3/constants';
import { FaCheck } from 'react-icons/fa6';
import { Chain } from '@src/types';

export interface IDesktopWalletConnectProps {
  isOpen: boolean;
  onClose: () => void;
  isLoading: boolean;
  pendingConnector?: Connector;
  connectors: Connector[];
  connect: (args: { connector: Connector }) => void;
  desiredChain?: Chain;
}

const DisconnectButton = () => {
  const { disconnect } = useDisconnect();
  return (
    <Button
      onClick={() => disconnect()}
      color="red"
      borderColor="red"
      bgColor="transparent"
      borderRadius="40px"
      h="50px"
      mt={4}
    >
      Disconnect
    </Button>
  );
};

export const WalletConnectModal = memo(
  ({ isOpen, onClose, isLoading, pendingConnector, connectors, connect, desiredChain }: IDesktopWalletConnectProps) => {
    const { connector: activeConnector, isConnected } = useAccount();
    const { switchNetwork } = useSwitchNetwork();

    const selectWallet = async (connector: Connector) => {
      await connect({ connector });

      if (desiredChain) {
        console.log(`🔄 Переключаем сеть на ${desiredChain.name} (${desiredChain.chainId})`);
        switchNetwork?.(desiredChain.chainId);
      }

      onClose();
    };

    return (
      <DialogRoot open={isOpen} onOpenChange={onClose} size="xs" placement="center">
        <DialogContent maxW="375px" borderRadius={{ base: '12px', md: '30px' }} backdrop>
          <DialogHeader>
            <DialogTitle textAlign="center" fontSize="18px" mb={0} fontWeight={700}>
              Connect a wallet
            </DialogTitle>
            <DialogCloseTrigger borderRadius="30px" />
          </DialogHeader>
          <DialogBody>
            <Flex flexDir="column" gap="10px">
              {connectors.map((connector: Connector) => {
                const loading = isLoading && connector.id === pendingConnector?.id;
                const isActive = connector.id === activeConnector?.id;

                return (
                  <Button
                    key={connector.id}
                    loading={loading}
                    onClick={() => selectWallet(connector)}
                    variant="outline"
                    w="100%"
                    display="flex"
                    justifyContent="space-between"
                    alignItems="center"
                    borderColor={{ base: 'light', _dark: 'darkLine' }}
                    bgColor={{ base: 'white', _dark: '#192B43' }}
                    borderRadius="40px"
                    h="50px"
                  >
                    <Flex align="center" gap={2}>
                      <NextImage width={16} height={16} src={walletConfig[connector.id]?.iconSrc} alt="icon" />
                      {connector.name}
                    </Flex>
                    {isActive && <Icon as={FaCheck} color="green.500" />}
                  </Button>
                );
              })}
              {isConnected && <DisconnectButton />}
            </Flex>
          </DialogBody>
        </DialogContent>
      </DialogRoot>
    );
  },
);

WalletConnectModal.displayName = 'ConnectWalletModal';
