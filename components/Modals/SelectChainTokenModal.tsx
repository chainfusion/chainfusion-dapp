import React, { useState, useEffect } from 'react';
import { getSupportedChains, getSupportedTokens } from '@src/config';
import { Chain, Token } from '@src/types';
import {
  DialogRoot,
  DialogContent,
  DialogHeader,
  DialogBody,
  DialogFooter,
  DialogTitle,
  DialogCloseTrigger,
} from '@src/components/ui/dialog';
import { Button } from '@src/components/ui/button';
import { Input, Box, Text, HStack, Image, createListCollection } from '@chakra-ui/react';
import { SelectRoot, SelectTrigger, SelectValueText, SelectContent, SelectItem } from '@/components/ui/select';
import { InputGroup } from '@/components/ui/input-group';
import { LuSearch } from 'react-icons/lu';

interface SelectChainTokenModalProps {
  show: boolean;
  title: string;
  initialChain: Chain;
  initialToken: Token;
  close: () => void;
  select: (chain: Chain, token: Token) => void;
}

const SelectChainTokenModal = ({
  show,
  title,
  initialChain,
  initialToken,
  close: parentClose,
  select,
}: SelectChainTokenModalProps) => {
  const chains = getSupportedChains();
  const tokens = getSupportedTokens();

  const [selectedChain, setSelectedChain] = useState<Chain>(initialChain);
  const [selectedToken, setSelectedToken] = useState<Token>(initialToken);

  const [chainFilter, setChainFilter] = useState('');
  const [tokenFilter, setTokenFilter] = useState('');

  useEffect(() => {
    setSelectedChain(initialChain);
    setSelectedToken(initialToken);
  }, [initialChain, initialToken]);

  const resetFilter = () => {
    setChainFilter('');
    setTokenFilter('');
  };

  const close = () => {
    resetFilter();
    parentClose();
  };

  const chainsList = createListCollection({
    items: chains,
    itemToString: (item) => item.name,
    itemToValue: (item) => item.identifier,
  });

  const tokensList = createListCollection({
    items: tokens,
    itemToString: (item) => `${item.name} (${item.symbol})`,
    itemToValue: (item) => item.identifier,
  });

  return (
    <DialogRoot open={show} onOpenChange={close} size="sm" placement="center">
      <DialogContent maxW="300px" borderRadius="30px" backdrop>
        <DialogHeader px="15px" pb={0}>
          <DialogTitle textAlign="center" fontSize="18px" fontWeight="700" mb={0}>
            Select <strong>{title}</strong>
          </DialogTitle>
          <DialogCloseTrigger color="silver" />
        </DialogHeader>

        <DialogBody px="15px">
          <Box mb={4}>
            <Text as="label" color={{ base: 'grayTwo', _dark: '#8694A5' }} fontSize=".8125rem" fontWeight="500">
              Blockchain:
            </Text>
            <SelectRoot
              size="sm"
              width="full"
              collection={chainsList}
              value={[selectedChain.identifier]}
              onValueChange={(valueDetails) => {
                const value = valueDetails.value[0];
                const newChain = chains.find((chain) => chain.identifier === value);
                if (newChain) {
                  setSelectedChain(newChain);
                }
              }}
              positioning={{ sameWidth: true }}
            >
              <SelectTrigger
                className="custom-select-trigger"
                border="1px solid"
                borderColor="light"
                borderRadius="full"
                height="50px"
                width="full"
                textAlign="left"
                fontWeight="500"
                fontSize="0.875rem"
                display="flex"
                alignItems="center"
                px="4px"
              >
                <HStack w="100%">
                  <Image alt={selectedChain.name} src={`/img/${selectedChain.identifier}.svg`} boxSize="20px" />
                  <SelectValueText placeholder="Select Blockchain">
                    {(items) => (items.length ? items[0].name : 'Select Blockchain')}
                  </SelectValueText>
                  {/*<Icon as={FaChevronDown} />*/}
                </HStack>
              </SelectTrigger>
              <SelectContent portalled={false} borderRadius="25px" p="20px">
                <Box py={2}>
                  <InputGroup flex="1" startElement={<LuSearch />}>
                    <Input
                      size="sm"
                      placeholder="Search..."
                      bgColor="light2"
                      borderColor="light"
                      outline="none"
                      fontSize=".875rem"
                      borderRadius="12px"
                      value={chainFilter}
                      onChange={(e) => setChainFilter(e.target.value)}
                    />
                  </InputGroup>
                </Box>
                {chains
                  .filter(
                    (chain) =>
                      chain.name.toLowerCase().includes(chainFilter.toLowerCase()) ||
                      chain.identifier.toLowerCase().includes(chainFilter.toLowerCase()),
                  )
                  .map((chain) => (
                    <SelectItem
                      key={chain.identifier}
                      item={chain}
                      justifyContent="flex-start"
                      onSelect={() => setSelectedChain(chain)}
                      border="1px solid"
                      borderColor="light"
                      mt="3px"
                      h="33px"
                      p="6px 10px"
                      borderRadius="9px"
                      cursor="pointer"
                      _hover={{
                        borderColor: 'lightBlue',
                        bg: 'transparent',
                      }}
                    >
                      <HStack w="100%">
                        <Image alt={chain.name} src={`/img/${chain.identifier}.svg`} boxSize="20px" />
                        {chain.name}
                      </HStack>
                    </SelectItem>
                  ))}
              </SelectContent>
            </SelectRoot>
          </Box>

          <Box>
            <Text as="label" color={{ base: 'grayTwo', _dark: '#8694A5' }} fontSize=".8125rem" fontWeight="500">
              Token:
            </Text>
            <SelectRoot
              className="custom-select-trigger"
              collection={tokensList}
              value={[selectedToken.identifier]}
              onValueChange={(valueDetails) => {
                const value = valueDetails.value[0];
                const newToken = tokens.find((token) => token.identifier === value);
                if (newToken) {
                  setSelectedToken(newToken);
                }
              }}
              positioning={{ sameWidth: true }}
            >
              <SelectTrigger
                className="custom-select-trigger"
                border="1px solid"
                borderColor="light"
                borderRadius="full"
                height="50px"
                width="full"
                textAlign="left"
                fontWeight="500"
                fontSize="0.875rem"
                display="flex"
                alignItems="center"
                px="4px"
              >
                <HStack w="100%">
                  <Image alt={selectedToken.name} src={`/img/${selectedToken.identifier}.svg`} boxSize="20px" />
                  <SelectValueText placeholder="Select Token">
                    {(items) => (items.length ? `${items[0].name} (${items[0].symbol})` : 'Select Token')}
                  </SelectValueText>
                </HStack>
              </SelectTrigger>
              <SelectContent portalled={false} borderRadius="25px" p="20px">
                <Box py={2}>
                  <InputGroup flex="1" startElement={<LuSearch />}>
                    <Input
                      size="sm"
                      placeholder="Search..."
                      bgColor="light2"
                      borderColor="light"
                      outline="none"
                      fontSize=".875rem"
                      borderRadius="12px"
                      value={tokenFilter}
                      onChange={(e) => setTokenFilter(e.target.value)}
                    />
                  </InputGroup>
                </Box>
                {tokens
                  .filter(
                    (token) =>
                      token.name.toLowerCase().includes(tokenFilter.toLowerCase()) ||
                      token.symbol.toLowerCase().includes(tokenFilter.toLowerCase()),
                  )
                  .map((token) => (
                    <SelectItem
                      key={token.identifier}
                      item={token}
                      justifyContent="flex-start"
                      onSelect={() => setSelectedToken(token)}
                      border="1px solid"
                      borderColor="light"
                      mt="3px"
                      h="33px"
                      p="6px 10px"
                      borderRadius="9px"
                      cursor="pointer"
                      _hover={{
                        borderColor: 'lightBlue',
                        bg: 'transparent',
                      }}
                    >
                      <HStack>
                        <Image alt={token.name} src={`/img/${token.identifier}.svg`} boxSize="20px" />
                        {token.symbol} - {token.name}
                      </HStack>
                    </SelectItem>
                  ))}
              </SelectContent>
            </SelectRoot>
          </Box>
        </DialogBody>

        <DialogFooter display="grid" gridTemplateColumns="1fr 1fr" px="15px" pt={0}>
          <Button variant="outline" borderRadius="full" h="45px" onClick={close}>
            Cancel
          </Button>
          <Button
            variant="solid"
            borderRadius="full"
            h="45px"
            onClick={() => {
              select(selectedChain, selectedToken);
              close();
            }}
          >
            Done
          </Button>
        </DialogFooter>
      </DialogContent>
    </DialogRoot>
  );
};

export default SelectChainTokenModal;
