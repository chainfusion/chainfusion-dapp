import React, { useState } from 'react';
import {
  DialogRoot,
  DialogContent,
  DialogHeader,
  DialogBody,
  DialogFooter,
  DialogTitle,
  DialogCloseTrigger,
} from '@src/components/ui/dialog';
import { Button } from '@src/components/ui/button';
import { Input, Textarea, Box, Text, HStack, createListCollection } from '@chakra-ui/react';
import { SelectRoot, SelectTrigger, SelectValueText, SelectContent, SelectItem } from '@/components/ui/select';
import { InputGroup } from '@/components/ui/input-group';
import { LuSearch } from 'react-icons/lu';

interface SlashingProposalModalProps {
  show: boolean;
  close: () => void;
}

const validatorAddresses = [
  '0x0661F0297bC6234d8b31782Cd1926EC101dF2d27',
  '0x1234F0297bC6234d8b31782Cd1926EC101dF2d27',
  '0x5678F0297bC6234d8b31782Cd1926EC101dF2d27',
  '0x9ABCDEF0297bC6234d8b31782Cd1926EC101dF2d27',
  '0xDEADBEEF297bC6234d8b31782Cd1926EC101dF2d27',
];

const SlashingProposalModal = ({ show, close }: SlashingProposalModalProps) => {
  const [selectedAddress, setSelectedAddress] = useState(validatorAddresses[0]);
  const [filter, setFilter] = useState('');

  const resetFilter = () => {
    setFilter('');
  };

  const validatorList = createListCollection({
    items: validatorAddresses.map((addr) => ({ id: addr, name: addr })),
    itemToString: (item) => item.name,
    itemToValue: (item) => item.id,
  });

  return (
    <DialogRoot open={show} onOpenChange={close} size="sm" placement="center">
      <DialogContent maxW="500px" borderRadius="30px" backdrop>
        <DialogHeader px="20px">
          <DialogTitle textAlign="center" fontSize="18px" fontWeight="700">
            Create a Proposal
          </DialogTitle>
          <DialogCloseTrigger color="silver" />
        </DialogHeader>

        <DialogBody px="20px">
          <Box mb={4}>
            <Text as="label" color={{ base: 'grayTwo', _dark: '#8694A5' }} fontSize=".8125rem" fontWeight="500">
              Reason:
            </Text>
            <Textarea
              minH="90px"
              borderRadius="20px"
              autoresize
              size="xl"
              placeholder="Please write the text of the reason..."
            />
          </Box>

          <Box mt="5">
            <Text as="label" color={{ base: 'grayTwo', _dark: '#8694A5' }} fontSize=".8125rem" fontWeight="500">
              Select validator address:
            </Text>
            <SelectRoot
              size="sm"
              width="full"
              collection={validatorList}
              value={[selectedAddress]}
              onValueChange={(valueDetails) => {
                const value = valueDetails.value[0];
                if (validatorAddresses.includes(value)) {
                  setSelectedAddress(value);
                }
              }}
              positioning={{ sameWidth: true }}
            >
              <SelectTrigger
                className="custom-select-trigger"
                border="1px solid"
                borderColor="light"
                borderRadius="full"
                height="50px"
                width="full"
                textAlign="left"
                fontWeight="500"
                fontSize="0.875rem"
                display="flex"
                alignItems="center"
                px="4px"
              >
                <HStack w="100%">
                  <SelectValueText placeholder="Select Validator">
                    {(items) => (items.length ? items[0].name : 'Select Validator')}
                  </SelectValueText>
                </HStack>
              </SelectTrigger>
              <SelectContent portalled={false} borderRadius="25px" p="15px">
                <Box py={2} w="100%">
                  <InputGroup w="100%" flex="1" startElement={<LuSearch />}>
                    <Input
                      size="sm"
                      placeholder="Search..."
                      bgColor="light2"
                      borderColor="light"
                      outline="none"
                      fontSize=".875rem"
                      w="full"
                      borderRadius="12px"
                      value={filter}
                      onChange={(e) => setFilter(e.target.value)}
                    />
                  </InputGroup>
                </Box>
                {validatorAddresses
                  .filter((addr) => addr.toLowerCase().includes(filter.toLowerCase()))
                  .map((addr) => (
                    <SelectItem
                      key={addr}
                      item={{ id: addr, name: addr }}
                      justifyContent="flex-start"
                      onSelect={() => {
                        resetFilter();
                        setSelectedAddress(addr);
                      }}
                      border="1px solid"
                      borderColor="light"
                      mt="3px"
                      h="33px"
                      p="6px 10px"
                      borderRadius="9px"
                      cursor="pointer"
                      _hover={{
                        borderColor: 'lightBlue',
                        bg: 'transparent',
                      }}
                    >
                      <HStack w="100%" fontFamily="var(--font-noto-sans-mono)">
                        {addr}
                      </HStack>
                    </SelectItem>
                  ))}
              </SelectContent>
            </SelectRoot>
          </Box>
        </DialogBody>

        <DialogFooter display="grid" gridTemplateColumns="1fr 1fr" px="20px" pt={0}>
          <Button variant="outline" borderRadius="full" h="45px" onClick={close}>
            Cancel
          </Button>
          <Button variant="solid" borderRadius="full" h="45px" onClick={close}>
            Create
          </Button>
        </DialogFooter>
      </DialogContent>
    </DialogRoot>
  );
};

export default SlashingProposalModal;
