import { useState, useEffect } from 'react';
import { useBridgeStore } from '@store/bridge';
import { utils } from 'ethers';
// import { useWeb3React } from '@web3-react/core';
import {
  DialogRoot,
  DialogContent,
  DialogHeader,
  DialogBody,
  DialogFooter,
  DialogTitle,
  DialogCloseTrigger,
} from '@src/components/ui/dialog';
import { Button } from '@src/components/ui/button';
import { Input, Text, Box } from '@chakra-ui/react';
import { useAccount } from 'wagmi';

interface OptionsModalProps {
  show: boolean;
  close: () => void;
}

const OptionsModal = ({ show, close }: OptionsModalProps) => {
  // const { account } = useWeb3React();
  const { address } = useAccount();
  const { receiver, setReceiver } = useBridgeStore();

  const [receiverOption, setReceiverOption] = useState<string>(receiver ?? '');

  useEffect(() => {
    setReceiverOption(receiver ?? '');
  }, [receiver]);

  return (
    <DialogRoot open={show} onOpenChange={close} size="sm" placement="center">
      <DialogContent borderRadius="30px" backdrop>
        <DialogHeader>
          <DialogTitle textAlign="center" fontSize="18px" fontWeight={700}>
            Options
          </DialogTitle>
          <DialogCloseTrigger color="silver" />
        </DialogHeader>

        <DialogBody mb={0} pb={4}>
          <Box mb="0">
            <Text as="label" color="gray.500" fontSize="sm" fontWeight="500">
              Receiver:
            </Text>
            <Input
              type="text"
              id="receiver"
              value={receiverOption}
              onChange={(e) => setReceiverOption(e.target.value)}
              placeholder={address ?? 'Enter transfer receiver address'}
              fontWeight="500"
              fontSize="0.875rem"
              bg="white"
              height="50px"
              border="1px solid"
              borderColor="light"
              borderRadius="full"
              boxShadow="none"
              px="15px"
              outline="none"
              transition="all ease-out 0.3s"
              _hover={{
                bg: 'rgba(231, 238, 245, 0.52)',
                border: '1px solid var(--chakra-colors-lightBlue)',
              }}
              _focus={{
                bg: 'rgba(231, 238, 245, 0.52)',
                border: '1px solid var(--chakra-colors-lightBlue)',
              }}
              css={{
                '::placeholder': { color: 'var(--chakra-colors-silver)' },
              }}
            />
          </Box>
        </DialogBody>

        <DialogFooter display="grid" gridTemplateColumns="1fr 1fr">
          <Button
            variant="outline"
            borderRadius="full"
            h="45px"
            onClick={() => {
              setReceiverOption(receiver ?? '');
              close();
            }}
          >
            Cancel
          </Button>

          <Button
            variant="solid"
            borderRadius="full"
            h="45px"
            onClick={() => {
              if (utils.isAddress(receiverOption.toLowerCase()) || receiverOption === '') {
                setReceiver(receiverOption);
              } else {
                console.log('Не сработало(');
                setReceiverOption('');
              }
              close();
            }}
          >
            Save
          </Button>
        </DialogFooter>
      </DialogContent>
    </DialogRoot>
  );
};

export default OptionsModal;
