import React, { useState } from 'react';
import {
  DialogRoot,
  DialogContent,
  DialogHeader,
  DialogBody,
  DialogFooter,
  DialogTitle,
  DialogCloseTrigger,
} from '@src/components/ui/dialog';
import { Button } from '@src/components/ui/button';
import { Input, Text, Flex, Box, Image } from '@chakra-ui/react';
import { BigNumber, utils } from 'ethers';
import { trimDecimals } from '@src/utils';
import { FaCheck } from 'react-icons/fa';

interface InputCFNModalProps {
  show: boolean;
  decimals: number;
  maxValue: BigNumber;
  maxValueText: string;
  title: string;
  buttonText: string;
  submit: (amount: BigNumber) => void;
  close: () => void;
}

const InputCFNModal = ({
  show,
  decimals,
  maxValue,
  maxValueText,
  title,
  buttonText,
  submit,
  close,
}: InputCFNModalProps) => {
  const [amount, setAmount] = useState<string>('');
  let inputValid = false;

  try {
    const parsedAmount = utils.parseUnits(amount, decimals);
    if (parsedAmount.gt(0) && parsedAmount.lte(maxValue)) {
      inputValid = true;
    }
  } catch {
    // ignore errors
  }

  const closeModal = () => {
    close();
    setAmount('');
  };

  return (
    <DialogRoot open={show} onOpenChange={closeModal} size="sm" placement="center">
      <DialogContent borderRadius="30px" backdrop>
        <DialogHeader>
          <DialogTitle textAlign="center" fontSize="18px" fontWeight={700}>
            {title}
          </DialogTitle>
          <DialogCloseTrigger color="silver" />
        </DialogHeader>

        <DialogBody>
          <Box mb="10px">
            <Text as="label" color="gray.500" fontSize="sm" fontWeight="500">
              Amount:
            </Text>
            <Flex position="relative">
              {BigNumber.isBigNumber(maxValue) && maxValue.gt(0) && (
                <Box
                  as="button"
                  onClick={() => setAmount(utils.formatUnits(maxValue, decimals))}
                  display="flex"
                  alignItems="center"
                  justifyContent="center"
                  cursor="pointer"
                  position="absolute"
                  top="5px"
                  right="6px"
                  fontWeight="600"
                  fontSize="0.75rem"
                  textAlign="center"
                  width="40px"
                  height="40px"
                  bg="rgba(13, 126, 231, 0.1)"
                  color="main"
                  borderRadius="50px"
                  transition="all ease-out 0.1s"
                  _hover={{ opacity: 0.9 }}
                  zIndex={1}
                >
                  Max
                </Box>
              )}
              <Input
                type="number"
                step="any"
                id="amount"
                value={amount}
                onChange={(e) => setAmount(e.target.value)}
                placeholder="Please write amount (CFN)..."
                fontWeight="500"
                fontSize="1rem"
                bg="transparent"
                height="50px"
                border="1px solid var(--chakra-colors-light)"
                borderRadius="50px"
                boxShadow="none"
                px="15px"
                outline="none"
                transition="all ease-out 0.3s"
                _hover={{
                  bg: 'rgba(231, 238, 245, 0.52)',
                  border: '1px solid var(--chakra-colors-light-blue)',
                }}
                _focus={{
                  bg: 'rgba(231, 238, 245, 0.52)',
                  border: '1px solid var(--chakra-colors-light-blue)',
                }}
                css={{
                  '::-webkit-outer-spin-button': { display: 'none' },
                  '::-webkit-inner-spin-button': { display: 'none' },
                  '-moz-appearance': 'textfield',
                }}
              />
            </Flex>
          </Box>

          {BigNumber.isBigNumber(maxValue) && maxValue.gt(0) && (
            <Flex align="center" mt="2">
              <Text fontSize="sm" color="gray.500" fontWeight="500" mb={0} mr="2">
                {maxValueText}:
              </Text>
              <Box
                as="button"
                color="main"
                fontWeight="600"
                onClick={() => setAmount(utils.formatUnits(maxValue, decimals))}
              >
                {utils.formatUnits(trimDecimals(maxValue, decimals, 8), decimals)}
              </Box>
              <Flex align="center" ml="2">
                <Image src="/img/cfn.svg" alt="CFN" boxSize="20px" />
                <Text ml="1" fontSize="sm" fontWeight="500" color={{ base: 'dark', _dark: 'light' }} mb={0}>
                  CFN
                </Text>
              </Flex>
            </Flex>
          )}
        </DialogBody>

        <DialogFooter display="grid" gridTemplateColumns="1fr 1fr">
          <Button variant="outline" h="45px" onClick={closeModal} borderRadius="full">
            Cancel
          </Button>
          <Button
            variant="solid"
            borderRadius="full"
            h="45px"
            disabled={!inputValid}
            onClick={() => {
              submit(utils.parseUnits(amount, decimals));
              closeModal();
            }}
          >
            <FaCheck style={{ width: '14px' }} />
            {buttonText}
          </Button>
        </DialogFooter>
      </DialogContent>
    </DialogRoot>
  );
};

export default InputCFNModal;
