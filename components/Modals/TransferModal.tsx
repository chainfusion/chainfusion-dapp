import React from 'react';
import {
  DialogRoot,
  DialogContent,
  DialogHeader,
  DialogBody,
  DialogTitle,
  DialogCloseTrigger,
} from '@src/components/ui/dialog';
import { Box, Flex, Text, Link, Icon, Spinner, ProgressCircle, AbsoluteCenter } from '@chakra-ui/react';
import { FaCheckCircle } from 'react-icons/fa';
import { FiExternalLink } from 'react-icons/fi';

interface TransferModalProps {
  show: boolean;
  stage: number;
  stageLinks: Map<number, string>;
  close: () => void;
}

const TransferModal = ({ show, stage, stageLinks, close }: TransferModalProps) => {
  const percent = Math.min(Math.round(((stage - 1) / 3) * 100), 100);

  return (
    <DialogRoot open={show} onOpenChange={close} size="sm" placement="center">
      <DialogContent borderRadius="30px" backdrop>
        <DialogHeader>
          <DialogTitle textAlign="center" fontSize="18px" fontWeight="700">
            {stage < 4 ? 'Transferring Tokens' : 'Done!'}
          </DialogTitle>
          <DialogCloseTrigger color="silver" />
        </DialogHeader>

        <DialogBody>
          <Flex justify="center" align="center" mb="4">
            <ProgressCircle.Root value={percent} size="xl">
              <ProgressCircle.Circle>
                <ProgressCircle.Track />
                <ProgressCircle.Range />
              </ProgressCircle.Circle>
              <AbsoluteCenter>
                <ProgressCircle.ValueText color="main" />
              </AbsoluteCenter>
            </ProgressCircle.Root>
          </Flex>

          {/* Progress List */}
          <Box as="ul" listStyleType="none" m={0} p={0} gap={3}>
            <ProgressItem
              stage={1}
              activeStage={stage}
              link={stageLinks.get(1)}
              message="Confirming deposit transaction"
            />
            <ProgressItem stage={2} activeStage={stage} link={stageLinks.get(2)} message="Registering transfer event" />
            <ProgressItem
              stage={3}
              activeStage={stage}
              link={stageLinks.get(3)}
              message="Receiving transfer on destination chain"
            />
          </Box>
        </DialogBody>
      </DialogContent>
    </DialogRoot>
  );
};

interface ProgressItemProps {
  stage: number;
  activeStage: number;
  link: string | undefined;
  message: string;
}

const ProgressItem = ({ stage, activeStage, link, message }: ProgressItemProps) => {
  return (
    <Box
      as="li"
      display="flex"
      alignItems="center"
      gap="10px"
      fontSize="0.875rem"
      color={stage >= activeStage ? '#a6abbd' : 'dark'}
      border="1px solid"
      borderColor="light"
      borderRadius="50px"
      minH="45px"
      p="10px 15px"
      mb="5px"
      position="relative"
      transition="all ease-out 0.3s"
    >
      {stage < activeStage ? (
        <Icon as={FaCheckCircle} color="success" fontSize="1rem" position="absolute" left="15px" />
      ) : (
        <Spinner color="main" size="sm" position="absolute" left="15px" />
      )}

      <Text flex="1" pl="25px" mb={0}>
        {message}
      </Text>
      {link && (
        <Link
          href={link}
          target="_blank"
          rel="noreferrer"
          color="var(--main-color)"
          fontSize="0.875rem"
          position="absolute"
          right="16px"
          transition="all ease-out 0.3s"
          _hover={{ color: 'hover' }}
        >
          <Icon as={FiExternalLink} />
        </Link>
      )}
    </Box>
  );
};

export default TransferModal;
