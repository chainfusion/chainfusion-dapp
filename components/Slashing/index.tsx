'use client';

import { useState } from 'react';
import SlashingProposalModal from '@components/Modals/SlashingProposalModal';
import { getNativeChain } from '@src/config';
import { Container, Box, Flex, Icon, Link, Stack, Text, HStack } from '@chakra-ui/react';
import { Button } from '@src/components/ui/button';
import { FaCheck } from 'react-icons/fa6';
import { IoHandRightOutline } from 'react-icons/io5';
import { PiCalendarPlusLight, PiClockLight } from 'react-icons/pi';
import { GoPlusCircle } from 'react-icons/go';
import { Tooltip } from '@/components/ui/tooltip';
import { ClipboardIconButton, ClipboardRoot } from '@/components/ui/clipboard';
import { FiCopy } from 'react-icons/fi';
import {
  PaginationRoot,
  PaginationPrevTrigger,
  PaginationNextTrigger,
  PaginationItems,
} from '@/components/ui/pagination';

const getStatusStyles = (status: string) => {
  switch (status) {
    case 'Pending':
      return { bg: 'warningLight', color: 'warning' };
    case 'Executed':
      return { bg: 'light5', color: 'success' };
    case 'Expired':
      return { bg: 'errorLight', color: 'error' };
    default:
      return { bg: 'gray.100', color: 'gray.500' };
  }
};

const slashingData = [
  {
    status: 'Pending',
    action: 'Slashing',
    description: 'Validator attempted to sign an invalid block',
    address: '0xA1B2C3D4E5F67890123456789ABCDEF012345678',
    votes: '5 / 15',
    createdAt: '10.02.2024 - 12:45',
    expiresIn: '6 days',
    hasVoted: false,
  },
  {
    status: 'Pending',
    action: 'Slashing',
    description: 'Validator attempted to sign an invalid block ...',
    address: '0xA1B2C3D4E5F67890123456789ABCDEF012345678',
    votes: '7 / 15',
    createdAt: '10.02.2024 - 14:25',
    expiresIn: '4 days',
    hasVoted: true,
  },
  {
    status: 'Executed',
    action: 'Ban',
    description: 'Repeated failure to follow consensus rules',
    address: '0xF9E8D7C6B5A43210987654321FEDCBA987654321',
    votes: '12 / 15',
    createdAt: '09.02.2024 - 08:30',
    executedAt: '14.02.2024 - 14:22',
    hasVoted: true,
  },
  {
    status: 'Expired',
    action: 'Slashing',
    description: 'Failure to submit required transactions in time',
    address: '0x11223344556677889900AABBCCDDEEFF00112233',
    votes: '3 / 15',
    createdAt: '08.02.2024 - 10:15',
    expiredAt: '13.02.2024 - 09:50',
    hasVoted: false,
  },
  {
    status: 'Pending',
    action: 'Slashing',
    description: 'Validator did not participate in consensus voting',
    address: '0x44556677889900AABBCCDDEEFF11223344556677',
    votes: '6 / 15',
    createdAt: '11.02.2024 - 17:40',
    expiresIn: '5 days',
    hasVoted: true,
  },
  {
    status: 'Executed',
    action: 'Ban',
    description: 'Unauthorized double signing detected',
    address: '0xABCDEF0123456789ABCDEF0123456789ABCDEF01',
    votes: '14 / 15',
    createdAt: '07.02.2024 - 22:15',
    executedAt: '12.02.2024 - 16:00',
    hasVoted: false,
  },
  {
    status: 'Expired',
    action: 'Slashing',
    description: 'Validator node was offline for extended period',
    address: '0x789ABCDEF0123456789ABCDEF0123456789ABCD',
    votes: '2 / 15',
    createdAt: '05.02.2024 - 14:20',
    expiredAt: '10.02.2024 - 13:55',
    hasVoted: true,
  },
  {
    status: 'Pending',
    action: 'Slashing',
    description: 'Attempted modification of blockchain state',
    address: '0x56789ABCDEF0123456789ABCDEF0123456789AB',
    votes: '7 / 15',
    createdAt: '12.02.2024 - 08:00',
    expiresIn: '4 days',
    hasVoted: false,
  },
  {
    status: 'Executed',
    action: 'Ban',
    description: 'Validator used an unauthorized key pair',
    address: '0x99AABBCCDDEEFF00112233445566778899001122',
    votes: '9 / 15',
    createdAt: '06.02.2024 - 20:10',
    executedAt: '11.02.2024 - 15:45',
    hasVoted: true,
  },
  {
    status: 'Expired',
    action: 'Slashing',
    description: 'Repeatedly failed to produce blocks',
    address: '0xEEFF001122334455667788990011223344556677',
    votes: '4 / 15',
    createdAt: '04.02.2024 - 09:30',
    expiredAt: '09.02.2024 - 08:15',
    hasVoted: false,
  },
];

const ITEMS_PER_PAGE = 3;
const totalPages = Math.ceil(slashingData.length / ITEMS_PER_PAGE);

export default function SlashingClient() {
  const [showSlashingProposalModal, setShowSlashingProposalModal] = useState(false);
  const [activePage, setActivePage] = useState(1);
  const nativeChain = getNativeChain();

  const [data, setData] = useState(slashingData);
  const paginatedData = data.slice((activePage - 1) * ITEMS_PER_PAGE, activePage * ITEMS_PER_PAGE);

  const toggleVote = (index: number) => {
    setData((prev) => prev.map((item, i) => (i === index ? { ...item, hasVoted: !item.hasVoted } : item)));
  };

  return (
    <>
      <Box as="section" pt={{ base: '100px', lg: '3rem' }} pb="1.5rem">
        <Container>
          <Flex justifyContent="flex-end">
            <Button
              variant="outline"
              size="medium"
              borderRadius="full"
              onClick={() => setShowSlashingProposalModal(true)}
            >
              <Icon as={GoPlusCircle} /> Create Proposal
            </Button>
          </Flex>

          <Stack gap={4} mt={4}>
            {paginatedData.map((item, index) => {
              const globalIndex = (activePage - 1) * ITEMS_PER_PAGE + index;
              const { bg, color } = getStatusStyles(item.status);

              return (
                <Flex
                  key={index}
                  alignItems="center"
                  p="15px 18px"
                  bg={{ base: 'white', _dark: '#192B43' }}
                  border="1px solid"
                  borderColor={{ base: 'light', _dark: 'darkLine' }}
                  borderRadius="15px"
                  filter="drop-shadow(0 10px 20px rgba(75,105,139,.03))"
                  flexDir={{ base: 'column-reverse', lg: 'row' }}
                >
                  <Flex
                    as="button"
                    onClick={() => toggleVote(globalIndex)}
                    alignItems="center"
                    justifyContent="center"
                    w="43px"
                    h="43px"
                    p={3}
                    borderRadius="50%"
                    bg={item.hasVoted ? 'main' : 'light'}
                    color={item.hasVoted ? 'white' : 'main'}
                  >
                    <Icon as={IoHandRightOutline} fontSize="lg" />
                  </Flex>
                  <Box
                    flex={1}
                    borderBottom={{ base: '1px solid var(--chakra-colors-light)', lg: 0 }}
                    borderLeft={{ base: 0, lg: '1px solid var(--chakra-colors-light)' }}
                    ml={{ base: 0, lg: 4 }}
                    pl={{ base: 0, lg: 4 }}
                    mb={{ base: 4, lg: 0 }}
                    pb={{ base: 4, lg: 0 }}
                  >
                    <Flex justifyContent="space-between" alignItems="center" w="100%">
                      <Text
                        fontSize=".75rem"
                        fontWeight="500"
                        bg={bg}
                        color={color}
                        border="1px solid"
                        borderColor={color}
                        px={3}
                        py={1}
                        borderRadius="6px"
                        display="inline-block"
                        mb={0}
                      >
                        {item.status}
                      </Text>
                      {item.hasVoted && (
                        <Text
                          fontSize=".75rem"
                          fontWeight="500"
                          bg="light3"
                          color={{ base: 'grayTwo', _dark: '#8694A5' }}
                          px={3}
                          py={1}
                          borderRadius="md"
                          display="inline-block"
                          mb={0}
                        >
                          <Icon as={FaCheck} mr={1} /> You have voted
                        </Text>
                      )}
                    </Flex>
                    <Text fontSize="sm" fontWeight="600" mt={2} mb={0}>
                      {item.action}:{' '}
                      <Text as="span" fontWeight="400" color={{ base: 'grayTwo', _dark: '#8694A5' }}>
                        {item.description}
                      </Text>
                    </Text>
                    <Text fontSize="sm" fontWeight="600" mt={1} mb={0}>
                      Address:{' '}
                      <Flex display="inline-flex" alignItems="center">
                        <Link
                          href={nativeChain.explorer}
                          fontWeight={400}
                          fontFamily="var(--font-noto-sans-mono)"
                          color="blue.500"
                        >
                          {item.address}
                        </Link>
                        <ClipboardRoot value={item.address}>
                          <Tooltip content="Copy to clipboard" showArrow openDelay={50} closeDelay={100}>
                            <ClipboardIconButton
                              display="inline-flex"
                              p={0}
                              fontSize="12px"
                              w="14px"
                              h="14px"
                              minW="auto"
                              ml="3px"
                              color={{ base: 'grayTwo', _dark: '#8694A5' }}
                              icon={<FiCopy style={{ width: '12px', height: '12px' }} />}
                            />
                          </Tooltip>
                        </ClipboardRoot>
                      </Flex>
                    </Text>

                    <Text fontSize="sm" fontWeight="600" mt={1} mb={0}>
                      Votes:{' '}
                      <Text
                        as="span"
                        bgColor="light3"
                        fontSize=".8125rem"
                        color="main"
                        border="1px solid"
                        borderColor="light1"
                        borderRadius="6px"
                        p="3px 7px"
                        fontWeight="600"
                      >
                        {item.votes}
                      </Text>
                    </Text>
                    <Text
                      display="flex"
                      alignItems="center"
                      gap="3px"
                      fontSize=".8125rem"
                      fontWeight="600"
                      mt={1}
                      mb={0}
                    >
                      Created at:{' '}
                      <Text
                        as="span"
                        display="inline-flex"
                        alignItems="center"
                        gap="3px"
                        fontWeight={400}
                        color={{ base: 'grayTwo', _dark: '#8694A5' }}
                      >
                        <Icon as={PiCalendarPlusLight} /> {item.createdAt}
                      </Text>
                    </Text>
                    {item.expiresIn && (
                      <Text
                        display="flex"
                        alignItems="center"
                        gap="3px"
                        fontSize=".8125rem"
                        fontWeight="600"
                        mt={1}
                        mb={0}
                      >
                        Expires in:{' '}
                        <Text
                          as="span"
                          display="inline-flex"
                          alignItems="center"
                          gap="3px"
                          fontWeight={400}
                          color={{ base: 'grayTwo', _dark: '#8694A5' }}
                        >
                          <Icon as={PiClockLight} /> {item.expiresIn}
                        </Text>
                      </Text>
                    )}
                    {item.executedAt && (
                      <Text
                        display="flex"
                        alignItems="center"
                        gap="3px"
                        fontSize=".8125rem"
                        fontWeight="600"
                        mt={1}
                        mb={0}
                      >
                        Executed at:{' '}
                        <Text
                          as="span"
                          display="inline-flex"
                          alignItems="center"
                          gap="3px"
                          fontWeight={400}
                          color={{ base: 'grayTwo', _dark: '#8694A5' }}
                        >
                          <Icon as={PiClockLight} /> {item.executedAt}
                        </Text>
                      </Text>
                    )}
                    {item.expiredAt && (
                      <Text
                        display="flex"
                        alignItems="center"
                        gap="3px"
                        fontSize=".8125rem"
                        fontWeight="600"
                        mt={1}
                        mb={0}
                      >
                        Expired at:{' '}
                        <Text
                          as="span"
                          display="inline-flex"
                          alignItems="center"
                          gap="3px"
                          fontWeight={400}
                          color={{ base: 'grayTwo', _dark: '#8694A5' }}
                        >
                          <Icon as={PiClockLight} /> {item.expiredAt}
                        </Text>
                      </Text>
                    )}
                  </Box>
                </Flex>
              );
            })}
          </Stack>

          <Stack gap="8" mt={6}>
            <PaginationRoot
              count={totalPages}
              pageSize={1}
              defaultPage={activePage}
              onPageChange={({ page }) => setActivePage(page)}
            >
              <HStack justifyContent="space-between" alignItems="center">
                <Flex gap="6px">
                  <PaginationPrevTrigger />
                  <PaginationItems />
                  <PaginationNextTrigger />
                </Flex>

                <Text fontSize="sm" fontWeight="500">
                  Page{' '}
                  <Text as="span" fontWeight="600">
                    {activePage}
                  </Text>{' '}
                  of{' '}
                  <Text as="span" fontWeight="600">
                    {totalPages}
                  </Text>
                </Text>
              </HStack>
            </PaginationRoot>
          </Stack>
        </Container>
      </Box>

      {/* Slashing Modal */}
      <SlashingProposalModal show={showSlashingProposalModal} close={() => setShowSlashingProposalModal(false)} />
    </>
  );
}
