'use client';

import NextLink from 'next/link';
import { Box, Container, Flex, HStack, Link as ChakraLink, Text } from '@chakra-ui/react';
import { FaTelegramPlane, FaMediumM, FaLinkedinIn } from 'react-icons/fa';
import { FaXTwitter } from 'react-icons/fa6';
import { getNativeChain } from '@src/config';
import { socialLinks } from '@src/utils/config';

const nativeChain = getNativeChain();

export const Footer = () => {
  return (
    <Box as="footer" w="100%" py={4} bg={{ base: 'white', _dark: 'dark' }} color="white" mt="auto">
      <Container>
        <Flex direction={{ base: 'column', md: 'row' }} gap="26px" align="center" justify="space-between">
          <Text fontSize="xs" color={{ base: 'grayTwo', _dark: '#8694A5' }} mb={0}>
            ChainFusion © {new Date().getFullYear()}
          </Text>

          <HStack
            as="nav"
            gap="15px"
            wrap="wrap"
            justify="center"
            fontSize="xs"
            m={{ base: '0 auto', md: '0 auto 0 0' }}
            fontWeight={600}
          >
            {[
              { href: 'https://chainfusion.org', label: 'About' },
              { href: nativeChain.explorer, label: 'Explorer' },
              { href: 'https://docs.chainfusion.org', label: 'Documentation' },
              { href: '/', label: 'Data Privacy' },
            ].map((link) => (
              <ChakraLink
                as={NextLink}
                key={link.href}
                href={link.href}
                _hover={{ textDecoration: 'underline' }}
                color={{ base: 'grayThree', _dark: 'light3' }}
              >
                {link.label}
              </ChakraLink>
            ))}
          </HStack>

          <HStack gap={4}>
            {socialLinks.map((social, index) => {
              const icons = [FaXTwitter, FaTelegramPlane, FaMediumM, FaLinkedinIn];
              const IconComponent = icons[index] || FaXTwitter;

              return (
                <ChakraLink
                  key={social.href}
                  href={social.href}
                  w="26px"
                  h="26px"
                  borderRadius="50%"
                  bgColor={{ base: 'light3', _dark: '#C1CBF94D' }}
                  color={{ base: 'grayTwo', _dark: '#8694A5' }}
                  display="flex"
                  alignItems="center"
                  justifyContent="center"
                  outline="none"
                  _hover={{
                    color: { base: 'dark', _dark: 'white' },
                  }}
                  target="_blank"
                >
                  <IconComponent />
                </ChakraLink>
              );
            })}
          </HStack>
        </Flex>
      </Container>
    </Box>
  );
};
