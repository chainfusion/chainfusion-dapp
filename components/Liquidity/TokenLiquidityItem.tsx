import { MockToken__factory } from '@chainfusion/erc-20-bridge-contracts';
import { useChainContext } from '@src/context/ChainContext';
import { Chain, Token } from '@src/types';
import { trimDecimals } from '@src/utils';
import { BigNumber, utils } from 'ethers';
import { useEffect, useState } from 'react';

import { Box, Grid, Text, Button, Skeleton, IconButton, Image } from '@chakra-ui/react';
import { FaPlus, FaMinus, FaArrowsRotate } from 'react-icons/fa6';
import { useAccount, useNetwork } from 'wagmi';

export interface TokenLiquidityItemProps {
  chain: Chain;
  token: Token;

  onAddLiquidity: (token: Token, maxAmount: BigNumber) => void;
  onRemoveLiquidity: (token: Token, maxAmount: BigNumber) => void;
}

export const TokenLiquidityItem = ({ chain, token, onAddLiquidity, onRemoveLiquidity }: TokenLiquidityItemProps) => {
  const { networkContainer, signerAccount, switchNetwork } = useChainContext();
  const { chain: currentChain } = useNetwork();
  const { isConnected: isActive } = useAccount();

  const chainId = currentChain?.id;

  const [isLoading, setIsLoading] = useState(true);
  const [refreshPending, setRefreshPending] = useState(false);
  const [collectPending, setCollectPending] = useState(false);
  const [ourBalance, setOurBalance] = useState(BigNumber.from(0));
  const [ourLiquidity, setOurLiquidity] = useState(BigNumber.from(0));
  const [ourRewards, setOurRewards] = useState(BigNumber.from(0));
  const [providedLiquidity, setProvidedLiquidity] = useState(BigNumber.from(0));
  const [availableLiquidity, setAvailableLiquidity] = useState(BigNumber.from(0));

  const refreshRewards = async () => {
    const network = networkContainer.get(chain.identifier);
    if (network?.contracts === undefined) {
      return;
    }

    const tokenAddress = token.chains[chain.identifier];
    if (tokenAddress === undefined) {
      return;
    }

    setRefreshPending(true);

    try {
      if (chainId !== chain.chainId) {
        await switchNetwork(chain);
      }

      await (await network.contracts.feeManager.distributeRewards(tokenAddress)).wait();
      setRefreshPending(false);
    } catch {
      setRefreshPending(false);
    }
  };

  const collectRewards = async () => {
    const network = networkContainer.get(chain.identifier);
    if (network?.contracts === undefined) {
      return;
    }

    const tokenAddress = token.chains[chain.identifier];
    if (tokenAddress === undefined) {
      return;
    }

    setCollectPending(true);

    try {
      if (chainId !== chain.chainId) {
        await switchNetwork(chain);
      }

      await (await network.contracts.liqidityPools.claimRewards(tokenAddress)).wait();
      setCollectPending(false);
    } catch {
      setCollectPending(false);
    }
  };

  useEffect(() => {
    let pending = true;

    if (networkContainer.size == 0) {
      return;
    }

    const network = networkContainer.get(chain.identifier);
    if (network?.contracts === undefined) {
      return;
    }

    const tokenAddress = token.chains[chain.identifier];
    if (tokenAddress === undefined) {
      return;
    }

    setProvidedLiquidity(BigNumber.from(0));
    setAvailableLiquidity(BigNumber.from(0));
    setOurLiquidity(BigNumber.from(0));
    setOurRewards(BigNumber.from(0));
    setIsLoading(true);

    const liquidityPools = network.contracts.liqidityPools;

    const loadLiqudity = async () => {
      const providedLiquidityPromise = liquidityPools.providedLiquidity(tokenAddress);
      const availableLiquidityPromise = liquidityPools.availableLiquidity(tokenAddress);
      const ourLiquidityPromise = liquidityPools.liquidityPositions(tokenAddress, signerAccount);
      const ourRewardsPromise = liquidityPools.rewardsOwing(tokenAddress);

      let balance = BigNumber.from(0);

      const network = networkContainer.get(chain.identifier);
      if (network !== undefined && network.contracts !== undefined) {
        const mockTokenFactory = new MockToken__factory(network.provider.getSigner(signerAccount));
        const mockToken = mockTokenFactory.attach(tokenAddress);
        const balancePromise = mockToken.balanceOf(network.account);

        balance = await balancePromise;
      }

      const providedLiquidity = await providedLiquidityPromise;
      const availableLiquidity = await availableLiquidityPromise;
      const ourLiquidity = await ourLiquidityPromise;
      const ourRewards = await ourRewardsPromise;

      if (pending) {
        setOurBalance(balance);
        setProvidedLiquidity(trimDecimals(providedLiquidity, token.decimals, 6));
        setAvailableLiquidity(trimDecimals(availableLiquidity, token.decimals, 6));
        setOurLiquidity(trimDecimals(ourLiquidity.balance, token.decimals, 6));
        setOurRewards(trimDecimals(ourRewards, token.decimals, 6));
        setIsLoading(false);
      }
    };

    loadLiqudity().then();

    return () => {
      pending = false;
    };
  }, [signerAccount, networkContainer, chain, token, refreshPending, collectPending]);

  return (
    <Grid
      templateColumns={{ base: '1fr', lg: '25% 1fr 1fr' }}
      p={{ base: '20px', lg: '13px' }}
      bg={{ base: 'white', _dark: '#192B43' }}
      border="1px solid"
      borderColor={{ base: 'light', _dark: 'darkLine' }}
      borderRadius="md"
      filter="drop-shadow(0px 10px 20px rgba(75, 105, 139, 0.03))"
      transition="all ease-out 0.1s"
    >
      <Box
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
        mb={{ base: '10px', lg: 0 }}
      >
        <Image src={`/img/${token.identifier}.svg`} alt="" w="45px" mb="1" />
        <Text fontWeight="600" fontSize="1rem" color={{ base: 'dark', _dark: 'light' }} mb={0}>
          {token.symbol}
        </Text>
      </Box>

      <Grid
        templateColumns={{ base: '1fr 1fr', lg: '1fr' }}
        borderTop={{
          base: {
            base: '1px solid var(--chakra-colors-light)',
            _dark: '1px solid var(--chakra-colors-dark-line)',
          },
          lg: {
            base: 0,
            _dark: 0,
          },
        }}
        borderLeft={{
          base: {
            base: 0,
            _dark: 0,
          },
          lg: {
            base: '1px solid var(--chakra-colors-light)',
            _dark: '1px solid var(--chakra-colors-dark-line)',
          },
        }}
        px={{ base: 0, lg: 5 }}
        py={{ base: '10px', lg: 0 }}
      >
        <Box
          display="flex"
          flexDirection="column"
          justifyContent="center"
          fontWeight="500"
          fontSize="0.8125rem"
          lineHeight="160%"
          color={{ base: 'grayTwo', _dark: '#8694A5' }}
        >
          <Text mb="3px" as="div">
            Provided:{' '}
            {isLoading ? (
              <Skeleton
                variant="shine"
                css={{
                  base: {
                    '--start-color': '#e4edf8',
                    '--end-color': '#e4edf87d',
                  },
                  _dark: {
                    '--start-color': '#2A3B55',
                    '--end-color': '#3B4E6B',
                  },
                }}
                height="12px"
                my="5px"
                borderRadius="30px"
                width="100%"
              />
            ) : (
              <Text
                as="span"
                display="block"
                fontWeight="700"
                fontSize="0.8125rem"
                color={{ base: 'dark', _dark: 'light' }}
              >
                {utils.formatEther(providedLiquidity)}{' '}
                <Text as="strong" fontWeight="500" fontSize="0.625rem" color={{ base: 'dark', _dark: 'light' }}>
                  {token.symbol}
                </Text>
              </Text>
            )}
          </Text>
        </Box>

        <Box
          display="flex"
          flexDirection="column"
          justifyContent="center"
          fontWeight="500"
          fontSize="0.8125rem"
          lineHeight="160%"
          color={{ base: 'grayTwo', _dark: '#8694A5' }}
        >
          <Text mb={0} as="div">
            Available:{' '}
            {isLoading ? (
              <Skeleton
                variant="shine"
                css={{
                  base: {
                    '--start-color': '#e4edf8',
                    '--end-color': '#e4edf87d',
                  },
                  _dark: {
                    '--start-color': '#2A3B55',
                    '--end-color': '#3B4E6B',
                  },
                }}
                height="12px"
                my="5px"
                borderRadius="30px"
                width="100%"
              />
            ) : (
              <Text
                as="span"
                display="block"
                fontWeight="700"
                fontSize="0.8125rem"
                color={{ base: 'dark', _dark: 'light' }}
              >
                {utils.formatEther(availableLiquidity)}{' '}
                <Text as="strong" fontWeight="500" fontSize="0.625rem" color={{ base: 'dark', _dark: 'light' }}>
                  {token.symbol}
                </Text>
              </Text>
            )}
          </Text>
        </Box>
      </Grid>

      <Grid
        templateColumns={{ base: '1fr 1fr', lg: '1fr' }}
        borderTop={{
          lg: {
            base: 0,
            _dark: 0,
          },
          base: {
            base: '1px solid var(--chakra-colors-light)',
            _dark: '1px solid var(--chakra-colors-dark-line)',
          },
        }}
        borderLeft={{
          base: {
            base: 0,
            _dark: 0,
          },
          lg: {
            base: '1px solid var(--chakra-colors-light)',
            _dark: '1px solid var(--chakra-colors-dark-line)',
          },
        }}
        pt={{ base: '10px', lg: 0 }}
        px={{ base: 0, lg: 5 }}
      >
        {/* You Provided */}
        <Box mb="1">
          <Text
            fontWeight="500"
            fontSize="0.8125rem"
            lineHeight="160%"
            color={{ base: 'grayTwo', _dark: '#8694A5' }}
            mb={0}
          >
            You Provided:
          </Text>
          {isLoading ? (
            <Skeleton
              variant="shine"
              css={{
                base: {
                  '--start-color': '#e4edf8',
                  '--end-color': '#e4edf87d',
                },
                _dark: {
                  '--start-color': '#2A3B55',
                  '--end-color': '#3B4E6B',
                },
              }}
              height="12px"
              my="5px"
              borderRadius="30px"
              width="100%"
            />
          ) : (
            isActive && (
              <Box display="flex" alignItems="center">
                <Text fontWeight="700" fontSize="0.8125rem" color="main" mr="1" mb={0}>
                  {utils.formatEther(ourLiquidity)}
                </Text>
                <IconButton
                  aria-label="Add Liquidity"
                  w="16px"
                  minW={0}
                  h="16px"
                  ml="1"
                  onClick={() => onAddLiquidity(token, ourBalance)}
                  color="main"
                  bg="light3"
                  border="1px solid"
                  borderColor="light1"
                  borderRadius="full"
                >
                  <FaPlus style={{ width: '10px', height: '10px' }} />
                </IconButton>

                <IconButton
                  aria-label="Remove Liquidity"
                  w="16px"
                  minW={0}
                  h="16px"
                  ml="1"
                  onClick={() => onRemoveLiquidity(token, ourLiquidity)}
                  color="main"
                  bg="light3"
                  border="1px solid"
                  borderColor="light1"
                  borderRadius="full"
                >
                  <FaMinus style={{ width: '10px', height: '10px' }} />
                </IconButton>
              </Box>
            )
          )}
        </Box>

        {/* Rewards */}
        <Box>
          <Text
            fontWeight="500"
            fontSize="0.8125rem"
            lineHeight="160%"
            color={{ base: 'grayTwo', _dark: '#8694A5' }}
            mb={0}
          >
            Rewards:
          </Text>
          {isLoading ? (
            <Skeleton
              variant="shine"
              css={{
                base: {
                  '--start-color': '#e4edf8',
                  '--end-color': '#e4edf87d',
                },
                _dark: {
                  '--start-color': '#2A3B55',
                  '--end-color': '#3B4E6B',
                },
              }}
              height="12px"
              my="5px"
              borderRadius="30px"
              width="100%"
            />
          ) : (
            <Box display="flex" alignItems="center">
              <Text fontWeight="700" fontSize="0.8125rem" color="main" mr="1" mb={0}>
                {utils.formatEther(ourRewards)}
              </Text>
              {isActive && (
                <IconButton
                  aria-label="Refresh Rewards"
                  w="16px"
                  minW={0}
                  h="16px"
                  ml="1"
                  onClick={refreshRewards}
                  disabled={refreshPending}
                  color="main"
                  bgColor="light3"
                  border="light1"
                  borderRadius="full"
                  _hover={{ bgColor: 'light', borderColor: 'lightBlue' }}
                >
                  <FaArrowsRotate style={{ width: '10px', height: '10px' }} />
                </IconButton>
              )}
              {isActive && ourRewards.gt(0) && (
                <Button
                  size="xs"
                  px="3"
                  fontWeight="500"
                  fontSize="0.6875rem"
                  lineHeight="140%"
                  color="white"
                  bgColor="main"
                  borderRadius="full"
                  onClick={collectRewards}
                  disabled={collectPending}
                  _hover={{ bgColor: 'light', borderColor: 'lightBlue' }}
                >
                  Collect {collectPending && <FaArrowsRotate className="fa-spinner ml-1" />}
                </Button>
              )}
            </Box>
          )}
        </Box>
      </Grid>
    </Grid>
  );
};
