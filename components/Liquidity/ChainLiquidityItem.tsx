'use client';

import { getSupportedTokens } from '@src/config';
import { useChainContext } from '@src/context/ChainContext';
import { Chain } from '@src/types';
import { trimDecimals } from '@src/utils';
import { BigNumber, utils } from 'ethers';
import { useEffect, useState } from 'react';
import { Grid, Flex, VStack, HStack, Image, Text, Skeleton, LinkBox, LinkOverlay } from '@chakra-ui/react';
import NextLink from 'next/link';

export interface ChainLiquidityItemProps {
  chain: Chain;
}

export const ChainLiquidityItem = ({ chain }: ChainLiquidityItemProps) => {
  const { networkContainer, signerAccount } = useChainContext();

  const [isLoading, setIsLoading] = useState(true);
  const [tvl, setTVL] = useState(BigNumber.from(0));

  const tokens = getSupportedTokens().filter((token) => token.chains[chain.identifier] !== undefined);

  useEffect(() => {
    let pending = true;

    if (networkContainer.size === 0) return;

    const network = networkContainer.get(chain.identifier);
    if (!network?.contracts) return;

    const liquidityPools = network.contracts.liqidityPools;

    const loadTVL = async () => {
      let tvl: BigNumber = BigNumber.from(0);
      const tokenTVLPromises: Promise<BigNumber>[] = [];

      for (const token of tokens) {
        const tokenAddress = token.chains[chain.identifier];
        if (!tokenAddress) continue;

        tokenTVLPromises.push(liquidityPools.availableLiquidity(tokenAddress));
      }

      for (const tvlPromise of tokenTVLPromises) {
        tvl = tvl.add(await tvlPromise);
      }

      if (pending) {
        setTVL(trimDecimals(tvl, 18, 2));
        setIsLoading(false);
      }
    };

    loadTVL().then();

    return () => {
      pending = false;
    };
  }, [signerAccount, networkContainer, chain, tokens]);

  return (
    <LinkBox
      as="article"
      p={4}
      borderWidth="1px"
      borderRadius="15px"
      shadow="sm"
      bg={{ base: 'white', _dark: '#192B43' }}
      borderColor={{ base: 'light', _dark: 'darkLine' }}
      boxShadow="0px 8px 14px rgba(75, 105, 139, 0.07)"
      transition="all .1s ease-out"
      _hover={{ borderColor: 'lightBlue' }}
    >
      <LinkOverlay as={NextLink} href={`/liquidity/${chain.identifier}`} />

      <Flex align="center" mb={4} gap="8px">
        <Image boxSize="20px" src={`/img/${chain.identifier}.svg`} alt={`${chain.name} Logo`} />
        <Text fontSize="18px" fontWeight="500" mb={0}>
          {chain.name} Pool
        </Text>
      </Flex>

      <Grid templateColumns="1fr 1fr" alignItems="center" gap="5px">
        <VStack
          align="start"
          gap={0}
          border="1px dashed"
          px="15px"
          borderColor={{ base: 'light', _dark: 'darkLine' }}
          borderRadius="10px"
          alignItems="flex-start"
          justifyContent="center"
          h="52px"
        >
          <Text fontSize=".75rem" color={{ base: 'grayTwo', _dark: '#8694A5' }} mb="3px">
            Tokens
          </Text>
          <HStack gap={1}>
            {tokens.slice(0, 3).map((token) => (
              <Image
                key={token.identifier}
                boxSize="17px"
                src={`/img/${token.identifier}.svg`}
                alt={token.identifier}
              />
            ))}
          </HStack>
        </VStack>

        <VStack
          border="1px dashed"
          px="15px"
          borderColor={{ base: 'light', _dark: 'darkLine' }}
          gap={0}
          borderRadius="10px"
          alignItems="flex-start"
          justifyContent="center"
          h="52px"
        >
          <Text fontSize=".75rem" color={{ base: 'grayTwo', _dark: '#8694A5' }} mb={0}>
            TVL
          </Text>
          <Text as="div" fontSize=".8125rem" fontWeight={700} m={0} w="100%">
            {isLoading ? (
              <Skeleton
                variant="shine"
                borderRadius="30px"
                css={{
                  base: {
                    '--start-color': '#e4edf8',
                    '--end-color': '#e4edf87d',
                  },
                  _dark: {
                    '--start-color': '#2A3B55',
                    '--end-color': '#3B4E6B',
                  },
                }}
                height="12px"
                width="100%"
                mt="3px"
              />
            ) : (
              `$${utils.commify(utils.formatEther(tvl))}`
            )}
          </Text>
        </VStack>
      </Grid>
    </LinkBox>
  );
};
