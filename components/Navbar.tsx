'use client';

import { useState } from 'react';
import { Box, Button, Grid, HStack, Icon, Image, Link, Container } from '@chakra-ui/react';
import NextLink from 'next/link';
import { FaBars } from 'react-icons/fa6';
import { navLinks } from '@src/utils/config';
import { usePathname } from 'next/navigation';
import { WalletMenu } from '@components/WalletMenu';

const Navbar = () => {
  const pathname = usePathname();

  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <Box as="nav" mt={{ base: '0', md: '35px' }} width="100%">
      <Container>
        <Grid
          mx="auto"
          templateColumns={{ base: '1fr auto', md: '1fr auto 1fr' }}
          alignItems="center"
          py={{ base: '.5rem', md: 0 }}
          bgColor={{ base: 'white', md: 'transparent' }}
          borderBottom={{ base: '1px solid', md: 0 }}
          borderColor="light"
          h={{ base: '67px', md: 'auto' }}
        >
          <Grid alignItems="center">
            <Link as={NextLink} href="/" outline="none">
              <Image src="/img/logo.svg" alt="ChainFusion" maxW={{ base: '150px', md: '200px' }} />
            </Link>
          </Grid>

          <HStack as="nav" gap={0} justifySelf="center" display={{ base: 'none', md: 'flex' }}>
            {navLinks.map(({ href, label, key }) => {
              const isActive = href === '/' ? pathname === '/' : pathname.startsWith(href);

              return (
                <Link
                  as={NextLink}
                  key={key}
                  href={href}
                  fontWeight="medium"
                  fontSize="16px"
                  color={{
                    base: isActive ? 'main' : 'grayTwo',
                    _dark: isActive ? 'main' : 'gray.400',
                  }}
                  bgColor={{
                    base: isActive ? 'light1' : 'transparent',
                    _dark: isActive ? '#C1CBF91A' : 'transparent',
                  }}
                  px="25px"
                  h="40px"
                  display="flex"
                  alignItems="center"
                  borderRadius="41px"
                  outline="none"
                  _hover={{
                    color: {
                      base: isActive ? 'white' : 'main',
                      _dark: isActive ? 'white' : 'lightBlue',
                    },
                    bg: {
                      base: isActive ? 'main' : 'transparent',
                      _dark: isActive ? 'blue.600' : '#C1CBF908',
                    },
                  }}
                >
                  {label}
                </Link>
              );
            })}
          </HStack>

          <HStack gap="10px" justify="flex-end">
            <WalletMenu />
            <Button
              onClick={() => setIsMenuOpen(!isMenuOpen)}
              display={{ base: 'flex', md: 'none' }}
              variant="ghost"
              aria-label="Open Menu"
            >
              <Icon as={FaBars} boxSize={6} />
            </Button>
          </HStack>
        </Grid>
      </Container>
    </Box>
  );
};

export default Navbar;
