import Bridge from './bridge/page';

export const metadata = {
  title: 'ChainFusion | Bridge',
  description: 'Bridge allows you to transfer assets between different blockchains in a safe and decentralized way',
};

export default Bridge;
