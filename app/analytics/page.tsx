import { Box, Container, Flex, Heading } from '@chakra-ui/react';

export const metadata = {
  title: 'ChainFusion | Analytics',
  description: 'Bridge stats and analytics',
};

export default function Analytics() {
  return (
    <Box py="50px">
      <Container>
        <Flex justify="center">
          <Box w="full" textAlign="center">
            <Heading size="md">Coming soon</Heading>
          </Box>
        </Flex>
      </Container>
    </Box>
  );
}
