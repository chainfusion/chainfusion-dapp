'use client';

import { ReactNode } from 'react';
import { WagmiConfig } from 'wagmi';
import { ChakraProvider, Flex } from '@chakra-ui/react';
import { system } from '@src/lib/chakra/theme';
import { ColorModeProvider } from '@src/components/ui/color-mode';

import Navbar from '@components/Navbar';
import { Footer } from '@components/Footer';
import { wagmiConfig } from '@/lib/web3/config';
import { ChainContextProvider } from '@src/context/ChainContext';

export function Providers({ children }: { children: ReactNode }) {
  return (
    <WagmiConfig config={wagmiConfig}>
      <ChakraProvider value={system}>
        <ColorModeProvider defaultTheme="dark">
          <ChainContextProvider>
            <Flex direction="column" minH="100vh">
              <Navbar />
              {children}
              <Footer />
            </Flex>
          </ChainContextProvider>
        </ColorModeProvider>
      </ChakraProvider>
    </WagmiConfig>
  );
}
