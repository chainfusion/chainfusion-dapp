import { Container, Flex, Box } from '@chakra-ui/react';
import TransferHistory from '@components/Bridge/TransferHistory';
import BridgeWidget from '@components/Bridge/BridgeWidget';

const Bridge = () => {
  return (
    <Container>
      <Flex direction="column" align="center">
        <Box w={{ base: 'full', md: '66%', lg: '50%' }} mx="auto">
          <BridgeWidget />
        </Box>
        <TransferHistory />
      </Flex>
    </Container>
  );
};

export default Bridge;
