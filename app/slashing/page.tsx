export const metadata = {
  title: 'ChainFusion | Slashing',
  description: 'Create, view and vote for validator slashing',
};

import SlashingClient from '@components/Slashing';

export default function SlashingPage() {
  return <SlashingClient />;
}
