import { Metadata } from 'next';
import LiquidityManagement from '@components/LiquidityManagement';
import { getChain } from '@src/config';

export async function generateMetadata({ params }: { params: Promise<{ identifier: string }> }): Promise<Metadata> {
  const { identifier } = await params;
  const chain = getChain(identifier);

  return {
    title: chain ? `ChainFusion | Liquidity | ${chain.name}` : 'ChainFusion | Liquidity',
    description: chain
      ? `Manage liquidity for ${chain.name} in the ChainFusion bridge.`
      : 'Manage bridge liquidity across blockchains.',
  };
}

export default async function LiquidityPage({ params }: { params: Promise<{ identifier: string }> }) {
  const { identifier } = await params;
  return <LiquidityManagement identifier={identifier} />;
}
