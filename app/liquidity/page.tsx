import { Box, Container, SimpleGrid } from '@chakra-ui/react';
import { ChainLiquidityItem } from '@components/Liquidity/ChainLiquidityItem';
import { getSupportedChains } from '@src/config';
import { Chain } from '@src/types';

export const metadata = {
  title: 'ChainFusion | Liquidity',
  description: 'Manage bridge liquidity in blockchains per token',
};

const Liquidity = () => {
  const supportedChains = getSupportedChains();
  const liquidityItems = supportedChains.map((chain: Chain) => (
    <ChainLiquidityItem key={chain.identifier} chain={chain} />
  ));

  return (
    <Box py="50px">
      <Container>
        <SimpleGrid columns={{ base: 1, sm: 2, md: 3 }} gap={{ base: 3, md: 4, lg: 6 }}>
          {liquidityItems}
        </SimpleGrid>
      </Container>
    </Box>
  );
};

export default Liquidity;
