import { ReactNode } from 'react';
import dayjs from 'dayjs';
import { Inter, Noto_Sans_Mono } from 'next/font/google';

import localizedFormat from 'dayjs/plugin/localizedFormat';
import { Providers } from './providers';

dayjs.extend(localizedFormat);

const inter = Inter({ subsets: ['latin'] });

const notoSansMono = Noto_Sans_Mono({
  subsets: ['latin'],
  weight: ['400', '700'],
  variable: '--font-noto-sans-mono',
});

export const metadata = {
  title: 'ChainFusion',
  description: 'Your trusted bridge between blockchains.',
  icons: {
    icon: '/img/favicon.svg',
  },
};

export default function RootLayout({ children }: { children: ReactNode }) {
  return (
    <html lang="en" className={`${notoSansMono.variable}`} suppressHydrationWarning>
      <body className={inter.className} suppressHydrationWarning>
        <Providers>{children}</Providers>
      </body>
    </html>
  );
}
