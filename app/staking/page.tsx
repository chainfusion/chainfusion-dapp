export const metadata = {
  title: 'ChainFusion | Staking',
  description: 'Stake CFN to validator transfers and receive rewards',
};

import Staking from '@components/Staking/StakingPage';

export default function StakingPage() {
  return <Staking />;
}
